ad_page_contract {
    Set the display mode for the specific acs_object_type.
    Takes an array (all the radio-buttons from the page),
    compares it with the currently configured settings and
    changes those settings that are different.
    
    @author Frank Bergmann (frank.bergmann@project-open.com)
    @cvs-id $Id: attribute-type-map-2.tcl,v 1.5 2015/11/25 16:58:10 cvs Exp $
} {
    acs_object_type:notnull
    return_url:notnull
    attrib:array
}

# --------------------------------------------------------------
# Defaults & Security

set user_id [auth::require_login]
set user_is_admin_p [im_is_user_site_wide_or_intranet_admin $user_id]
if {!$user_is_admin_p} {
    ad_return_complaint 1 "You have insufficient privileges to use this page"
    return
}

# --------------------------------------------------------------
# Compare the information from "attrib" and "hash"

foreach key [array names attrib] {

    # New value from parameter
    set new_val $attrib($key)

	if {[regexp {^([0-9]+)\.([0-9]+)$} $key match portlet_id object_type_id]} {

		if {"none" == $new_val} {
            db_dml delete_portlet_from_type_map "
            delete from webix_portlet_object_type_map
            where
                portlet_id = :portlet_id
                and object_type_id = :object_type_id"
        } else {

            set exists_p [db_string exists "select 1 from webix_portlet_object_type_map where portlet_id = :portlet_id and object_type_id = :object_type_id" -default 0]
            if {!$exists_p} {
                db_dml insert_dynfield_type_attribute_map "
                    insert into webix_portlet_object_type_map (
                        portlet_id, object_type_id
                    ) values (
                        :portlet_id, :object_type_id
                    )
                "
            }
	    }
	}
}


# --------------------------------------------------------------

# Remove all permission related entries in the system cache
im_permission_flush

ad_returnredirect $return_url



