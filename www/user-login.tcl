ad_page_contract {
    Return the user_id of the currently logged in user

    @param username username to use for trying to login the user
    @param password password to use
} {
    { username ""}
    { password ""}
}

if {$username ne "" && $password ne ""} {
    # Try to login the user
    array set auth_info [auth::authenticate -username $username -password $password]
    if {$auth_info(auth_status) ne "ok"} {
        im_rest_doc_return 200 "application/json" "{\"success\": false,\n\"message\": \"$auth_info(auth_message)\"\n}"
    } else {
        set user_id $auth_info(user_id)
    }
} else {
    set user_id [auth::get_user_id]
}

if {$user_id ne 0} {
    set token [im_generate_auto_login -user_id $user_id]
    set site_wide_admin_p [acs_user::site_wide_admin_p -user_id $user_id]
    set name [im_name_from_id $user_id]
    if {$site_wide_admin_p} {
        set swa "true"
    } else {
        set swa "false"
    }
    set logo_url [parameter::get_from_package_key -package_key webix-portal -parameter LogoURL]
    set favicon_url [parameter::get_from_package_key -package_key webix-portal -parameter FavIconUrl]
    set user_profiles [cog_rest::user_profile_objects -user_id $user_id]
    set locale [lang::user::locale -user_id $user_id]

    im_rest_doc_return 200 "application/json" "{\"success\": true,\n\"token\": \"$token\",\n\"message\": \"user logged in\",
        \"user_id\": $user_id, \n\"site_wide_admin\": $swa, \n\"locale\": $locale,
        \"profiles\": \[ [join $user_profiles ","] \]}"
} else {
    im_rest_doc_return 200 "application/json" "{\"success\": false,\n\"message\": \"user not logged in\"\n}"
    }
}
