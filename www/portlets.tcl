ad_page_contract {
    Overview of webix portlets

} {
} 


db_multirow -extend {portlet_name portlet_url} portlets portlets "select * from webix_portlets order by object_type, page_url, sort_order" {
    set portlet_name [_ webix-portal.portlet_${portlet_id}]
    set portlet_url [export_vars -base "portlet" -url {portlet_id}]
}

template::list::create -key portlet_id -name portlets -multirow portlets \
    -elements {
        portlet_id {
            label "ID"
        }
        portlet_name {
            display_col portlet_name
            label "Name"
            link_url_col portlet_url
            link_html { title "Change portlet configuration" }
        }
        object_type {
            label "Object Type"
        }
        page_url {
            label "URL"
        }
        sort_order {
            label "Sort Order"
        }
        location {
            label "Location"
        }
    }
