ad_page_contract {

    @author Malte Sussdorff malte.sussdorff@cognovis.de
    @creation-date 2022-05-04
    
} {    
    portlet_id:integer
	{return_url "/intranet"}
    {form_mode "display"}
}

# ******************************************************
# Initialization, defaults & security
# ******************************************************

set user_id [auth::require_login]
set user_is_admin_p [im_is_user_site_wide_or_intranet_admin $user_id]
if {!$user_is_admin_p} {
    ad_return_complaint 1 "[_ intranet-dynfield.You_have_insufficient_privileges_to_use_this_page]"
    return
}
set label_style "plain" 
set title "[_ webix-portal.Edit_Portlet]"

db_0or1row portlet_info "select * from webix_portlets where portlet_id = :portlet_id"
set portlet_name [cog::message::lookup -key "webix-portal.portlet_$portlet_id"]
set form_fields {
    {portlet_id:key}
}
lappend form_fields {
	portlet_name:text(inform)
	{label "Portlet Name"}
}

lappend form_fields {
	object_type:text 
	{label {<nobr>Object Type</nobr>}} 
	{html {size 30 maxlength 100}} 
	{mode "view"} 
}

lappend form_fields {
	page_url:text 
	{label {<nobr>Page Url</nobr>}} 
	{html {size 30 maxlength 100}} 
}

lappend form_fields {
	sort_order:text 
	{label {<nobr>Sort Order</nobr>}} 
	{html {size 5 maxlength 4}}
}

lappend form_fields {
	location:text 
	{label {<nobr>Location</nobr>}} 
	{html {size 30 maxlength 100}} 
}


lappend form_fields {
	portlet_type_id:text(im_category_tree)
	{label "Portlet Type"}
	{custom {category_type "Webix Portlet Type" translate_p 1 package_key "webix-portal"} } 
}

lappend form_fields {
	portlet_status_id:text(im_category_tree)
	{label "Portlet Status"}
	{custom {category_type "Webix Portlet Status" translate_p 1 package_key "webix-portal"} } 
}


ad_form \
    -name portlet_form \
    -form $form_fields \
    -mode $form_mode \
    -method POST \
    -new_request {
} -edit_request {    
} -edit_data {
	db_dml update "update webix_portlets set page_url = :page_url, sort_order = :sort_order, location=:location, 
		portlet_type_id = :portlet_type_id, portlet_status_id = :portlet_status_id where portlet_id = :portlet_id"

} -after_submit {


    # ------------------------------------------------------------------
    # Flush permissions
    # ------------------------------------------------------------------
    
    # Remove all permission related entries in the system cache
    im_permission_flush
}



# ------------------------------------------------------------------
# Includelet for permissions
# ------------------------------------------------------------------

#		     [list object_type $object_type] \


    set params [list \
		[list nomaster_p 1] \
		[list acs_object_type $object_type] \
    ]
    set perm_html [ad_parse_template -params $params "/packages/webix-portal/lib/portlet-permissions"]



    set map_html [ad_parse_template -params $params "/packages/webix-portal/lib/portlet-type-map"]
