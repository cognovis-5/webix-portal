SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.4.8-5.0.1.4.9.sql','');

--- Set Project Manager field to be uneditable
update im_dynfield_type_attribute_map set display_mode = 'display' where attribute_id = (select da.attribute_id from im_dynfield_attributes da, acs_attributes a where a.object_type = 'im_project' and da.acs_attribute_id = a.attribute_id and a.attribute_name = 'project_lead_id' limit 1) and object_type_id in(87, 89, 93, 95, 2505, 10000011, 10000014, 10000098, 10000391, 10000392, 10000808, 10000813);

-- Make skills uneditable
update im_freelance_skill_type_map set display_mode = 'display' where skill_type_id in (2000, 2002, 2004, 2014, 2024, 2028, 2016, 2020, 2022, 2026, 2006, 2008, 2010, 10000432) and object_type_id in(87, 89, 93, 95, 2505, 10000011, 10000014, 10000098, 10000391, 10000392, 10000808, 10000813);