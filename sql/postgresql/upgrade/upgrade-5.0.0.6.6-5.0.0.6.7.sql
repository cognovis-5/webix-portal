SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.6.6-5.0.0.6.7.sql','');
create or replace function inline_0 ()
returns integer as '
declare
        v_count integer;
begin
        select count(*) into v_count from user_tab_columns
        where lower(table_name) = ''webix_notifications'' and lower(column_name) = ''project_id'';
        IF v_count > 0 THEN return 0; END IF;

        alter table webix_notifications add column project_id integer references im_projects;

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function webix_notification__new (
    integer, varchar, integer, integer, integer,integer,text, integer
) returns integer as '
DECLARE
   p_creation_user        alias for $1;
   p_creation_ip        alias for $2;
   p_notification_type_id		alias for $3;
   p_notification_status_id	alias for $4;
   p_context_id         alias for $5;
   p_recipient_id       alias for $6;
   p_message       alias for $7;
   p_project_id     alias for $8;
   

    v_notification_id	integer;
BEGIN
    v_notification_id := acs_object__new (
        null,
        ''webix_notification'',
        now(),
        p_creation_user,
        p_creation_ip,
        p_context_id
    );

    insert into webix_notifications (
        notification_id, context_id, recipient_id,
        notification_type_id, notification_status_id, message, project_id
    ) values (
        v_notification_id, p_context_id, p_recipient_id,
        p_notification_type_id, p_notification_status_id, p_message, p_project_id
    );

    return v_notification_id;
end;' language 'plpgsql';