SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.6.7-5.0.0.6.8.sql','');


SELECT im_category_new ('14300', 'Mark as read', 'Webix Notification Action Type');
update im_categories set aux_string1 = 'Mark the current notification as read' where category_id = 14300;
update webix_notification_actions set action_type_id = 14300 where action_type_id = 4300;
update im_categories set category_id = 14300 where category_id = 4300;
update im_categories set aux_html2 = 'fas fa-check-square' where category_id = 14300;

update im_categories set category = 'Uplo' where category_id = 4301;
SELECT im_category_new ('14301', 'Upload Files', 'Webix Notification Action Type');
update webix_notification_actions set action_type_id = 14301 where action_type_id = 4301;
update im_categories set aux_html2 = 'fas fa-file-upload' where category_id = 14301;
update im_categories set aux_string1 = 'Upload file based on this notification' where category_id = 14301;
delete from im_categories where category_id = 4301;

SELECT im_category_new ('14302', 'Send E-Mail', 'Webix Notification Action Type');
update im_categories set aux_string1 = 'Send a mail (typically to the user causing this notification to occur)' where category_id = 14302;
update im_categories set aux_html2 = 'fas fa-paper-plane' where category_id = 14302;
update webix_notification_actions set action_type_id = 14302 where action_type_id = 4302;
delete from im_categories where category_id = 4302;

SELECT im_category_new ('14303', 'Open Object URL', 'Webix Notification Action Type');
update im_categories set aux_string1 = 'Open an object_url' where category_id = 14303;
update im_categories set aux_html2 = 'fas fa-external-link-square-alt' where category_id = 14303;
update webix_notification_actions set action_type_id = 14303 where action_type_id = 4303;
delete from im_categories where category_id = 4303;


SELECT im_category_new ('14304', 'Open Company', 'Webix Notification Action Type');
update im_categories set aux_string1 = 'Open the corresponding company profile' where category_id = 14304;
update im_categories set aux_html2 = 'fas fa-building' where category_id = 14304;
update webix_notification_actions set action_type_id = 14304 where action_type_id = 4304;
delete from im_categories where category_id = 4304;

SELECT im_category_new ('14305', 'Open Contact', 'Webix Notification Action Type');
update im_categories set aux_string1 = 'Open the corresponding contact profile' where category_id = 14305;
update im_categories set aux_html2 = 'fas fa-user' where category_id = 14305;
update webix_notification_actions set action_type_id = 14305 where action_type_id = 4305;
update im_categories set category_type = 'Intranet Project Type' where category_id = 4305;

update im_categories set category = '4306' where category_id = 4306;
SELECT im_category_new ('14306', 'Freelancer Selection', 'Webix Notification Action Type');
update im_categories set aux_string1 = 'Select a freelancer for the package' where category_id = 14306;
update webix_notification_actions set action_type_id = 14306 where action_type_id = 4306;
update im_categories set aux_html2 = 'fas fa-user-plus' where category_id = 14306;
delete from im_categories where category_id = 4306;

update im_categories set category = '4307' where category_id = 4307;
SELECT im_category_new ('14307', 'Download Files', 'Webix Notification Action Type');
update webix_notification_actions set action_type_id = 14307 where action_type_id = 4307;
update im_categories set aux_html2 = 'fas fa-file-download' where category_id = 14307;
update im_categories set aux_string1 = 'Download files based on this notification' where category_id = 14307;
delete from im_categories where category_id = 4307;


SELECT im_category_new ('14308', 'Direct CR Download', 'Webix Notification Action Type');
update im_categories set aux_string1 = 'Directly download the associated file to your desktop' where category_id = 14308;
update im_categories set aux_html2 = 'fas fa-file-download' where category_id = 14308;
update webix_notification_actions set action_type_id = 14308 where action_type_id = 4308;
delete from im_categories where category_id = 4308;

update im_categories set category = '4309' where category_id = 4309;
SELECT im_category_new ('14309', 'Open Assignment Overview', 'Webix Notification Action Type');
update im_categories set aux_string1 = 'Open the assignment in overview', aux_html2 = 'fas fa-file-upload' where category_id = 14309;
delete from im_categories where category_id = 4309;

update im_categories set category = '4310' where category_id = 4310;
SELECT im_category_new ('14310', 'View comments', 'Webix Notification Action Type');
update im_categories set aux_html2 = 'fas fa-comment-dots' where category_id = 14310;
delete from im_categories where category_id = 4310;
