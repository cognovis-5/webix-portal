SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.5.5-5.0.1.5.6.sql','');

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'webix_report_modules';
    IF 0 != v_count THEN return 0; END IF;

-- Create a new Object Type
    perform acs_object_type__create_type (
        'webix_report_module',	-- object_type
        'Webix report module',	-- pretty_name
        'Webix report modules',	-- pretty_plural
        'acs_object',		-- supertype
        'webix_report_modules',	-- table_name
        'report_module_id',		-- id_column
        'webix_report_module',	-- report_module_name
        'f',			-- abstract_p
        null,			-- type_extension_table
        'webix_report_module__name'	-- name_method
    );

    insert into acs_object_type_tables (object_type,table_name,id_column)
    values ('webix_report_module', 'webix_report_modules', 'report_module_id');

    update acs_object_types set
        status_type_table = 'webix_report_modules',
        status_column = 'report_module_status_id',
        type_column = 'report_module_type_id'
    where object_type = 'webix_report_module';

    create table webix_report_modules (
        report_module_id     integer
            constraint webix_report_module_pk
            primary key
            constraint webix_report_module_id_fk
            references acs_objects,
        report_module_type_id integer
            constraint webix_report_modules_type_fk
            references im_categories,
        report_module_status_id	integer
            constraint webix_report_modules_status_fk
            references im_categories,
        report_name varchar(100),
        config text
    );

    return 1;
end;
$$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

create or replace function webix_report_module__new (
    integer, varchar, integer, integer,varchar(100),text
) returns integer as '
DECLARE
   p_creation_user        alias for $1;
   p_creation_ip        alias for $2;
   p_report_module_type_id		alias for $3;
   p_report_module_status_id	alias for $4;
   p_report_name   alias for $5;
   p_config alias for $6;
   
    row             record;
    v_report_module_id	integer;
BEGIN
    v_report_module_id := acs_object__new (
        null,
        ''webix_report_module'',
        now(),
        p_creation_user,
        p_creation_ip,
        null
    );

    insert into webix_report_modules (
        report_module_id, report_module_type_id, report_module_status_id,
        report_name, config
    ) values (
        v_report_module_id, p_report_module_type_id, p_report_module_status_id,
        p_report_name, p_config
    );

    for row in
        select c.category_id
            from acs_object_types a, im_categories c
            where a.object_type = ''webix_report_module''
            and a.type_category_type = c.category_type
    loop
        insert into webix_report_module_object_type_map (report_module_id,object_type_id) values (v_report_module_id, row.category_id);
    end loop;

    return v_report_module_id;
end;' language 'plpgsql';


create or replace function webix_report_module__delete (integer) returns integer as '
DECLARE
    v_report_module_id	 alias for $1;
BEGIN

    delete from     webix_report_modules
    where	   report_module_id = v_report_module_id;

    -- Erase all the priviledges
    delete from     acs_permissions
    where	   object_id = v_report_module_id;

    PERFORM acs_object__delete(v_report_module_id);

    return 1;
end;' language 'plpgsql';


create or replace function webix_report_module__name (integer) returns varchar as '
DECLARE
    v_report_module_id    alias for $1;
    v_name	  varchar;
BEGIN
    select  report_name
    into    v_name
    from    webix_report_modules
    where   report_module_id = v_report_module_id;

    return v_name;
end;' language 'plpgsql';