SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.2.3-5.0.1.3.0.sql','');

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'webix_portlets';
    IF 0 != v_count THEN return 0; END IF;

-- Create a new Object Type
    perform acs_object_type__create_type (
        'webix_portlet',	-- object_type
        'Webix portlet',	-- pretty_name
        'Webix portlets',	-- pretty_plural
        'acs_object',		-- supertype
        'webix_portlets',	-- table_name
        'portlet_id',		-- id_column
        'webix_portlet',	-- portlet_name
        'f',			-- abstract_p
        null,			-- type_extension_table
        'webix_portlet__name'	-- name_method
    );

    insert into acs_object_type_tables (object_type,table_name,id_column)
    values ('webix_portlet', 'webix_portlets', 'portlet_id');

    update acs_object_types set
        status_type_table = 'webix_portlets',
        status_column = 'portlet_status_id',
        type_column = 'portlet_type_id'
    where object_type = 'webix_portlet';

    create table webix_portlets (
        portlet_id     integer
            constraint webix_portlet_pk
            primary key
            constraint webix_portlet_id_fk
            references acs_objects,
        page_url text,
        portlet_type_id integer
            constraint webix_portlets_type_fk
            references im_categories,
        object_type varchar(1000)
            constraint webix_portlet_object_type_fk
            references acs_object_types
            on delete cascade
            not null,
        portlet_status_id	integer
            constraint webix_portlets_status_fk
            references im_categories
            default 28550,
        sort_order integer,
        location varchar(100),
        unique(page_url,object_type,portlet_type_id)
    );

    -- Mapping table with action information

    create table webix_portlet_object_type_map (
        portlet_id integer
            constraint webix_notif_action_notif_fk
            references webix_portlets
            on delete cascade,
        object_type_id  integer
            constraint webix_notif_action_type_fk
            references im_categories
            on delete cascade,
        constraint webix_portlet_object_type_pk        
        primary key (portlet_id,object_type_id)
    );

    return 1;
end;
$$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

create or replace function webix_portlet__new (
    integer, varchar, integer, integer,varchar(1000),text
) returns integer as '
DECLARE
   p_creation_user        alias for $1;
   p_creation_ip        alias for $2;
   p_portlet_type_id		alias for $3;
   p_portlet_status_id	alias for $4;
   p_object_type alias for $5;
   p_page_url       alias for $6;
   
    row             record;
    v_portlet_id	integer;
BEGIN
    v_portlet_id := acs_object__new (
        null,
        ''webix_portlet'',
        now(),
        p_creation_user,
        p_creation_ip,
        null
    );

    insert into webix_portlets (
        portlet_id, page_url, object_type,
        portlet_type_id, portlet_status_id
    ) values (
        v_portlet_id, p_page_url, p_object_type,
        p_portlet_type_id, p_portlet_status_id
    );

    for row in
        select c.category_id
            from acs_object_types a, im_categories c
            where a.object_type = p_object_type
            and a.type_category_type = c.category_type
    loop
        insert into webix_portlet_object_type_map (portlet_id,object_type_id) values (v_portlet_id, row.category_id);
    end loop;

    return v_portlet_id;
end;' language 'plpgsql';


create or replace function webix_portlet__delete (integer) returns integer as '
DECLARE
    v_portlet_id	 alias for $1;
BEGIN

    -- Erase the im_trans_packages item associated with the id
    delete from     webix_portlet_object_type_map
    where	   portlet_id = v_portlet_id;

    delete from     webix_portlets
    where	   portlet_id = v_portlet_id;

    -- Erase all the priviledges
    delete from     acs_permissions
    where	   object_id = v_portlet_id;

    PERFORM acs_object__delete(v_portlet_id);

    return 0;
end;' language 'plpgsql';


create or replace function webix_portlet__name (integer) returns varchar as '
DECLARE
    v_portlet_id    alias for $1;
    v_name	  varchar;
BEGIN
    select  im_name_from_id(portlet_type_id) || '' ('' || object_type || '')''
    into    v_name
    from    webix_portlets
    where   portlet_id = v_portlet_id;

    return v_name;
end;' language 'plpgsql';

-- Type
SELECT im_category_new ('28500', 'CompanyBasicInfoPortlet', 'Webix Portlet Type');
SELECT im_category_new ('28501', 'CompanyProjects', 'Webix Portlet Type');
SELECT im_category_new ('28502', 'PriceListTable', 'Webix Portlet Type');
SELECT im_category_new ('28503', 'UserListPortlet', 'Webix Portlet Type');
SELECT im_category_new ('28504', 'MessagesPortlet', 'Webix Portlet Type');
SELECT im_category_new ('28505', 'ObjectNotesPortlet', 'Webix Portlet Type');
SELECT im_category_new ('28506', 'MainTranslatorsPortlet', 'Webix Portlet Type');
SELECT im_category_new ('28507', 'UserBasicInfoPortlet', 'Webix Portlet Type');
SELECT im_category_new ('28508', 'UserNotesPortlet', 'Webix Portlet Type');
SELECT im_category_new ('28509', 'UserProjects', 'Webix Portlet Type');
SELECT im_category_new ('28510', 'SkillsPortlet', 'Webix Portlet Type');



SELECT im_category_new ('28550', 'Enabled', 'Webix Portlet Status');
SELECT im_category_new ('28551', 'Disbled', 'Webix Portlet Status');