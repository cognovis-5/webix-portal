SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.5.4-5.0.0.5.5.sql','');

-- Create a new dynfield widget
SELECT im_dynfield_widget__new (
    null,                   -- widget_id
    'im_dynfield_widget',   -- object_type
    now(),                  -- creation_date
    null,                   -- creation_user
    null,                   -- creation_ip
    null,                   -- context_id
    'category_complexity_type',                -- widget_name
    'Complexity Type',  -- pretty_name
    'Complexity Types',  -- pretty_plural
    10007,                  -- storage_type_id
    'integer',              -- acs_datatype
    'im_category_tree',             -- widget
    'integer',              -- sql_datatype
    '{custom {category_type "Intranet Translation Complexity" include_empty_p 0}}',
    'im_name_from_id'
);

-- im_projects
-- Adding new column to im_projects
create or replace function inline_0()
returns integer as $$
DECLARE
	v_count		integer;
BEGIN
     SELECT count(column_name) into v_count
     FROM information_schema.columns
     WHERE table_name='im_projects' and column_name='complexity_type_id';

     IF v_count > 0 THEN return 1; END IF;

     ALTER TABLE im_projects ADD COLUMN complexity_type_id INTEGER NOT NULL DEFAULT 4290;
     return 1;
end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0();
DROP FUNCTION inline_0();

-- Adding dynfield
select im_dynfield_attribute_new(
    'im_project',
    'complexity_type_id',
    'Complexity Type',
    'category_complexity_type',
    'integer',
    't',
    '120',
    'f'
);

-- im_companies
-- Adding new column to im_companies
create or replace function inline_0()
returns integer as $$
DECLARE
	v_count		integer;
BEGIN
     SELECT count(column_name) into v_count
     FROM information_schema.columns
     WHERE table_name='im_companies' and column_name='default_complexity_type_id';

     IF v_count > 0 THEN return 1; END IF;

     ALTER TABLE im_companies ADD COLUMN default_complexity_type_id INTEGER NOT NULL DEFAULT 4290;
     return 1;
end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0();
DROP FUNCTION inline_0();

-- Adding dynfield
select im_dynfield_attribute_new(
    'im_company',
    'default_complexity_type_id',
    'Default Complexity Type',
    'category_complexity_type',
    'integer',
    't',
    '110',
    'f'
);