SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.4.5-5.0.1.4.6.sql','');

-- Layout and dynfields for im_project at '/#!/new-offer url

-- Layout --
create or replace function inline_0 ()
returns integer as '
    declare
            v_count_page_url integer;
    begin
        -- First we check if such page_url does not already exist for that object_type
        select count(*) into v_count_page_url
            from im_dynfield_layout_pages
            where page_url = ''/#!/new-offer'' 
            and object_type = ''im_project'' 
            and layout_type = ''table'';
        IF
            v_count_page_url = 0
        THEN
            -- We create layout page if it does not exist
            insert into im_dynfield_layout_pages (page_url, object_type, layout_type) values (''/#!/new-offer'', ''im_project'', ''table'');
        END IF;

        return v_count_page_url;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

-- im_project dynfield: project_name
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''im_project''
            and a.attribute_name = ''project_name'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_project'',
                ''project_name'',
                ''Project Name'',
                ''textbox_large'',
                ''string'',
                ''f'',
                5,
                ''f'',
                ''im_projects''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/new-offer'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/new-offer'', 5, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

-- im_project dynfield: customer (company_id in im_projects)
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''im_project''
            and a.attribute_name = ''company_id'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_project'',
                ''company_id'',
                ''Customer'',
                ''customers'',
                ''integer'',
                ''f'',
                10,
                ''f'',
                ''im_projects''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/new-offer'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/new-offer'', 10, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

-- im_project dynfield: project_type_id
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''im_project''
            and a.attribute_name = ''project_type_id'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_project'',
                ''project_type_id'',
                ''Project Type'',
                ''project_type'',
                ''integer'',
                ''f'',
                15,
                ''f'',
                ''im_projects''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/new-offer'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/new-offer'', 15, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

-- im_project dynfield: company_contact_id (customer contact)
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''im_project''
            and a.attribute_name = ''company_contact_id'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_project'',
                ''company_contact_id'',
                ''Company Contact'',
                ''customer_contact'',
                ''integer'',
                ''f'',
                20,
                ''f'',
                ''im_projects''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/new-offer'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/new-offer'', 20, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

-- im_project dynfield: parent project (parent_id in im_projects)
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''im_project''
            and a.attribute_name = ''parent_id'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_project'',
                ''parent_id'',
                ''Parent Project'',
                ''project_parent_options'',
                ''integer'',
                ''f'',
                25,
                ''f'',
                ''im_projects''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/new-offer'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/new-offer'', 25, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

-- im_project dynfield: final_company_id
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''im_project''
            and a.attribute_name = ''final_company_id'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_project'',
                ''final_company_id'',
                ''Final Company'',
                ''final_company'',
                ''integer'',
                ''f'',
                30,
                ''f'',
                ''im_projects''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/new-offer'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/new-offer'', 30, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

-- im_project dynfield: processing_time
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''im_project''
            and a.attribute_name = ''processing_time'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_project'',
                ''processing_time'',
                ''Processing Time'',
                ''textbox_small'',
                ''string'',
                ''f'',
                40,
                ''f'',
                ''im_projects''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/new-offer'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/new-offer'', 40, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();


-- im_project dynfield: source_language_id
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin

        -- First we create widget
        perform im_dynfield_widget__new (
	        null,''im_dynfield_widget'',now(),null,null,null,
	        ''single_language'',			-- widget_name
	        ''#webix-portal.Single Language#'',		-- pretty_name
	        ''#webix-portal.Single Language#'',		-- pretty_plural
	        10007,					-- storage_type_id
	        ''integer'',				-- acs_datatype
	        ''im_category_tree'',			-- widget
	        ''char(5)'',				-- sql_datatype
	        ''{custom {category_type "Intranet Translation Language"}}'' 	-- parameters
        );

            -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''im_project''
            and a.attribute_name = ''source_language_id'';     
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_project'',
                ''source_language_id'',
                ''Source Language'',
                ''single_language'',
                ''string'',
                ''f'',
                45,
                ''f'',
                ''im_projects''
            );            
        END IF;

        update im_dynfield_attributes set widget_name = ''single_language'' where attribute_id = v_new_attribute_id;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/new-offer'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/new-offer'', 45, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();



-- im_project dynfield: start_date
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''im_project''
            and a.attribute_name = ''start_date'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_project'',
                ''start_date'',
                ''Start Date'',
                ''datetime'',
                ''date'',
                ''f'',
                55,
                ''f'',
                ''im_projects''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/new-offer'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/new-offer'', 55, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

-- im_project dynfield: end_date
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''im_project''
            and a.attribute_name = ''end_date'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_project'',
                ''end_date'',
                ''End Date'',
                ''datetime'',
                ''date'',
                ''f'',
                60,
                ''f'',
                ''im_projects''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/new-offer'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/new-offer'', 60, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();


-- im_project dynfield: text_types (subject_area_id)
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''im_project''
            and a.attribute_name = ''subject_area_id'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_project'',
                ''subject_area_id'',
                ''Text Type'',
                ''category_subject_area'',
                ''integer'',
                ''f'',
                65,
                ''f'',
                ''im_projects''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/new-offer'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/new-offer'', 65, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();


-- im_project dynfield: company_project_nr
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''im_project''
            and a.attribute_name = ''company_project_nr'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_project'',
                ''company_project_nr'',
                ''Reference Number'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                75,
                ''f'',
                ''im_projects''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/new-offer'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/new-offer'', 75, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();


-- im_project dynfield: complexity_type_id
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''im_project''
            and a.attribute_name = ''complexity_type_id'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_project'',
                ''complexity_type_id'',
                ''Complexity Type'',
                ''category_complexity_type'',
                ''integer'',
                ''f'',
                80,
                ''f'',
                ''im_projects''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/new-offer'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/new-offer'', 80, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();