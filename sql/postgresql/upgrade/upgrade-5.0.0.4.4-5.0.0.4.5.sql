SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.4.4-5.0.0.4.5.sql','');

create or replace function inline_0()
returns integer as $$
DECLARE
	v_count		integer;
BEGIN
     SELECT count(column_name) into v_count
     FROM information_schema.columns
     WHERE table_name='im_projects' and column_name='contact_again_date';

     IF v_count > 0 THEN return 1; END IF;

     ALTER TABLE im_projects ADD COLUMN contact_again_date timestamptz;
     return 1;
end;
$$ LANGUAGE 'plpgsql';

SELECT inline_0();
DROP FUNCTION inline_0();