SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.5.0-5.0.1.5.1.sql','');


select create_menu ('webix-portal', 'webix_user_details', 'User Information', 'users-overview.user-details', 10, 'webix_companies_db', '', 'fa fa-user', 't',467);

select acs_permission__grant_permission((select menu_id from im_menus where url = 'users-overview.user-details' and package_name = 'webix-portal' limit 1), 465, 'write');
select acs_permission__grant_permission((select menu_id from im_menus where url = 'users-overview.user-details' and package_name = 'webix-portal' limit 1), 465, 'read');

select acs_permission__grant_permission((select menu_id from im_menus where url = 'users-overview.user-details' and package_name = 'webix-portal' limit 1), 461, 'write');
select acs_permission__grant_permission((select menu_id from im_menus where url = 'users-overview.user-details' and package_name = 'webix-portal' limit 1), 461, 'read');

select acs_permission__grant_permission((select menu_id from im_menus where url = 'users-overview.user-details' and package_name = 'webix-portal' limit 1), 467, 'write');
select acs_permission__grant_permission((select menu_id from im_menus where url = 'users-overview.user-details' and package_name = 'webix-portal' limit 1), 467, 'read');
