SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.3.0-5.0.0.3.1.sql','');

CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
declare
    v_count integer;
begin
    select count(*) into v_count
    from         user_tab_columns
where lower(table_name) = 'webix_notifications';
    IF 0 != v_count THEN return 0; END IF;

-- Create a new Object Type
    perform acs_object_type__create_type (
        'webix_notification',	-- object_type
        'Webix Notification',	-- pretty_name
        'Webix Notifications',	-- pretty_plural
        'acs_object',		-- supertype
        'webix_notifications',	-- table_name
        'notification_id',		-- id_column
        'webix-portal',	-- notification_name
        'f',			-- abstract_p
        null,			-- type_extension_table
        'webix_notification__name'	-- name_method
    );

    insert into acs_object_type_tables (object_type,table_name,id_column)
    values ('webix_notification', 'webix_notifications', 'notification_id');

    update acs_object_types set
        status_type_table = 'webix_notifications',
        status_column = 'notification_status_id',
        type_column = 'notification_type_id'
    where object_type = 'webix_notification';

    create table webix_notifications (
        notification_id     integer
            constraint webix_notification_pk
            primary key
            constraint webix_notification_id_fk
            references acs_objects,
        recipient_id        integer not null
            constraint webix_notif_user_fk
            references users,
        context_id           integer not null
            constraint webix_notif_object_fk
            references acs_objects,
        viewed_date         timestamptz,
        message             text,
        notification_type_id integer
                    constraint webix_notifications_type_fk
                    references im_categories,
        notification_status_id	integer
                    constraint webix_notifications_status_fk
                    references im_categories
    );

    -- Mapping table with action information

    create table webix_notification_actions (
        notification_id integer
            constraint webix_notif_action_notif_fk
            references webix_notifications,
        action_type_id  integer
            constraint webix_notif_action_type_fk
            references im_categories,
        action_object_id integer
            constraint webix_notif_action_object_fk
            references acs_objects,
        action_text     text,
        constraint webix_notif_action_pk        
        primary key (notification_id,action_type_id)
    );

    return 1;
end;
$$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

create or replace function webix_notification__new (
    integer, varchar, integer, integer, integer,integer,text
) returns integer as '
DECLARE
   p_creation_user        alias for $1;
   p_creation_ip        alias for $2;
   p_notification_type_id		alias for $3;
   p_notification_status_id	alias for $4;
   p_context_id         alias for $5;
   p_recipient_id       alias for $6;
   p_message       alias for $7;
   

    v_notification_id	integer;
BEGIN
    v_notification_id := acs_object__new (
        null,
        ''webix_notification'',
        now(),
        p_creation_user,
        p_creation_ip,
        p_context_id
    );

    insert into webix_notifications (
        notification_id, context_id, recipient_id,
        notification_type_id, notification_status_id, message
    ) values (
        v_notification_id, p_context_id, p_recipient_id,
        p_notification_type_id, p_notification_status_id, p_message
    );

    return v_notification_id;
end;' language 'plpgsql';


create or replace function webix_notification__delete (integer) returns integer as '
DECLARE
    v_notification_id	 alias for $1;
BEGIN

    -- Erase the im_trans_packages item associated with the id
    delete from     webix_notification_actions
    where	   notification_id = v_notification_id;

    delete from     webix_notifications
    where	   notification_id = v_notification_id;

    -- Erase all the priviledges
    delete from     acs_permissions
    where	   object_id = v_notification_id;

    PERFORM acs_object__delete(v_notification_id);

    return 0;
end;' language 'plpgsql';


create or replace function webix_notification__name (integer) returns varchar as '
DECLARE
    v_notification_id    alias for $1;
    v_name	  varchar;
BEGIN
    select  im_name_from_id(context_id) || '' - '' || im_name_from_id(notification_type_id) 
    into    v_name
    from    webix_notifications
    where   notification_id = v_notification_id;

    return v_name;
end;' language 'plpgsql';

-- Type
SELECT im_category_new ('4260', 'Assignment Overdue', 'Webix Notification Type');
SELECT im_category_new ('4261', 'Assignment Delivered', 'Webix Notification Type');
SELECT im_category_new ('4262', 'Assignment Accepted', 'Webix Notification Type');
SELECT im_category_new ('4263', 'Assignment Denied', 'Webix Notification Type');
SELECT im_category_new ('4264', 'Assignment Request Outstanding', 'Webix Notification Type');

SELECT im_category_new ('4265', 'Project Overdue', 'Webix Notification Type');
SELECT im_category_new ('4266', 'Quote Accepted', 'Webix Notification Type');
SELECT im_category_new ('4267', 'Quote Denied', 'Webix Notification Type');
SELECT im_category_new ('4268', 'Project Delivered', 'Webix Notification Type');

SELECT im_category_new ('4270', 'Package File Missing', 'Webix Notification Type');
SELECT im_category_new ('4271', 'Assignment Deadline Change', 'Webix Notification Type');

SELECT im_category_new ('4280', 'Default', 'Webix Notification Type');


SELECT im_category_new ('4240', 'Information', 'Webix Notification Status');
SELECT im_category_new ('4241', 'Warning', 'Webix Notification Status');
SELECT im_category_new ('4242', 'Important', 'Webix Notification Status');


-- Disable old scans using sencha
update apm_parameter_values set attr_value = 'f' where parameter_id = (select parameter_id from apm_parameters where parameter_name = 'ScanAndNotifyProjectLeadersOfInquiringStatuses' and package_key = 'sencha-freelance-translation');
update apm_parameter_values set attr_value = 'f' where parameter_id = (select parameter_id from apm_parameters where parameter_name = 'ScanForAcceptedAssignmentsWithoutPo' and package_key = 'sencha-freelance-translation');
update apm_parameter_values set attr_value = 'f' where parameter_id = (select parameter_id from apm_parameters where parameter_name = 'ScanForRfq' and package_key = 'sencha-freelance-translation');
update apm_parameter_values set attr_value = 'f' where parameter_id = (select parameter_id from apm_parameters where parameter_name = 'ScanOverdueAssignments' and package_key = 'sencha-freelance-translation');
update apm_parameter_values set attr_value = 'f' where parameter_id = (select parameter_id from apm_parameters where parameter_name = 'ScanOverdueProjects' and package_key = 'sencha-freelance-translation');
