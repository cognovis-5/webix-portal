SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.0.3-5.0.1.0.4.sql','');
drop table im_trans_participation;
drop table im_trans_rfq_answers;
drop table im_trans_rfqs;

alter table im_assignment_quality_report_ratings drop constraint im_assignq_report_id_fk;
alter table im_assignment_quality_report_ratings add constraint im_assignment_quality_report_id_fk foreign key (report_id) references im_assignment_quality_reports on delete cascade;

select create_menu('webix-portal', 'webix_freelancers_help', 'Help', 'https://www.kolibri.online/project-open/', 140, 'webix_main_menu', '[im_user_is_freelance_p $user_id]', 'fas fa-question-circle', 't', 465);
