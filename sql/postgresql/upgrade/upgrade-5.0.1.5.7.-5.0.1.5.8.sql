SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.5.7-5.0.1.5.8.sql','');

-- General chat
select create_menu ('webix-portal', 'webix_chat', 'Chat', 'chat.general-chat', 135, 'webix_main_menu', '', 'fa fa-message-dots', 't',459);
-- Project chats
select create_menu ('webix-portal', 'webix_project_chat', 'Messages', 'chat.project-chat', 110, 'webix_projects', '', 'fas fa-comments', 't',459);
