SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.4.1-5.0.1.4.2.sql','');

--- 
-- First fix page_urls from previous upgrade script
---

UPDATE im_dynfield_layout_pages 
SET page_url = '/#!/company' 
WHERE page_url = 'company' 
AND object_type = 'im_office'
AND NOT EXISTS (
    SELECT 1 
    FROM im_dynfield_layout_pages 
    WHERE page_url = '/#!/company' 
    AND object_type = 'im_office'
);

UPDATE im_dynfield_layout_pages 
SET page_url = '/#!/company' 
WHERE page_url = 'company' 
AND object_type = 'im_company'
AND NOT EXISTS (
    SELECT 1 
    FROM im_dynfield_layout_pages 
    WHERE page_url = '/#!/company' 
    AND object_type = 'im_company'
);

UPDATE im_dynfield_layout 
SET page_url = '/#!/company' 
WHERE page_url = 'company'
AND NOT EXISTS (
    SELECT 1 
    FROM im_dynfield_layout 
    WHERE page_url = '/#!/company'
);
----
-- Dynfields for CompanyCreationModal
----

-- First add new layouts

-- First add new layout for im_offices
create or replace function inline_0 ()
returns integer as '
    declare
            v_count_page_url integer;
    begin
        -- First we check if such page_url does not already exist for that object_type
        select count(*) into v_count_page_url
            from im_dynfield_layout_pages
            where page_url = ''/#!/companies'' 
            and object_type = ''im_office'' 
            and layout_type = ''table'';
        IF
            v_count_page_url = 0
        THEN
            -- We create layout page if it does not exist
            insert into im_dynfield_layout_pages (page_url, object_type, layout_type) values (''/#!/companies'', ''im_office'', ''table'');
        END IF;

        return v_count_page_url;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_count_page_url integer;
    begin
        -- First we check if such page_url does not already exist for that object_type
        select count(*) into v_count_page_url
            from im_dynfield_layout_pages
            where page_url = ''/#!/companies'' 
            and object_type = ''im_company'' 
            and layout_type = ''table'';
        IF
            v_count_page_url = 0
        THEN
            -- We create layout page if it does not exist
            insert into im_dynfield_layout_pages (page_url, object_type, layout_type) values (''/#!/companies'', ''im_company'', ''table'');
        END IF;

        return v_count_page_url;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();


create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''company_type_id'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_company'',
                ''company_type_id'',
                ''Name'',
                ''category_company_type'',
                ''string'',
                ''f'',
                5,
                ''f'',
                ''im_companies''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/companies'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/companies'', 5, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''company_name'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_company'',
                ''company_name'',
                ''Name'',
                ''textbox_small'',
                ''string'',
                ''f'',
                10,
                ''f'',
                ''im_companies''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/companies'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/companies'', 10, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''address_line1'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_office'',
                ''address_line1'',
                ''Address'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                15,
                ''f'',
                ''im_offices''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/companies'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/companies'', 15, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();


create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''address_postal_code'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_office'',
                ''address_postal_code'',
                ''Postal Code'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                20,
                ''f'',
                ''im_offices''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/companies'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/companies'', 20, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''address_city'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_office'',
                ''address_city'',
                ''City'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                25,
                ''f'',
                ''im_offices''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/companies'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/companies'', 25, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''address_state'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_office'',
                ''address_state'',
                ''State'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                30,
                ''f'',
                ''im_offices''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/companies'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/companies'', 30, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();


create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''address_country_code'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_office'',
                ''address_country_code'',
                ''Country'',
                ''country_codes'',
                ''string'',
                ''f'',
                35,
                ''f'',
                ''im_offices''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/companies'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/companies'', 35, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''phone'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_office'',
                ''phone'',
                ''Phone'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                40,
                ''f'',
                ''im_offices''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/companies'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/companies'', 40, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''fax'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_office'',
                ''fax'',
                ''Fax'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                45,
                ''f'',
                ''im_offices''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/companies'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/companies'', 45, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();


create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''default_referral_source_id'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_company'',
                ''default_referral_source_id'',
                ''Referral source'',
                ''category_referral_source'',
                ''string'',
                ''f'',
                50,
                ''f'',
                ''im_companies''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/companies'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/companies'', 50, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''vat_number'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_company'',
                ''vat_number'',
                ''VAT'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                55,
                ''f'',
                ''im_companies''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/companies'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/companies'', 55, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''payment_term_id'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_company'',
                ''payment_term_id'',
                ''Payment term'',
                ''payment_term'',
                ''string'',
                ''f'',
                60,
                ''f'',
                ''im_companies''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/companies'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/companies'', 60, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''default_quote_template_id'' order by attribute_id asc limit 1;      
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_company'',
                ''default_quote_template_id'',
                ''Default quote template'',
                ''cost_templates'',
                ''integer'',
                ''f'',
                65,
                ''f'',
                ''im_companies''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/companies'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/companies'', 65, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();


create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''default_invoice_template_id'' order by attribute_id asc limit 1;        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_company'',
                ''default_invoice_template_id'',
                ''Default invoice template'',
                ''cost_templates'',
                ''integer'',
                ''f'',
                75,
                ''f'',
                ''im_companies''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/companies'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/companies'', 75, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();


-- /#!/company-details (object type: person)

create or replace function inline_0 ()
returns integer as '
    declare
            v_count_page_url integer;
    begin
        -- First we check if such page_url does not already exist for that object_type
        select count(*) into v_count_page_url
            from im_dynfield_layout_pages
            where page_url = ''/#!/company-details'' 
            and object_type = ''person'' 
            and layout_type = ''table'';
        IF
            v_count_page_url = 0
        THEN
            -- We create layout page if it does not exist
            insert into im_dynfield_layout_pages (page_url, object_type, layout_type) values (''/#!/company-details'', ''person'', ''table'');
        END IF;

        return v_count_page_url;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''first_names'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''person'',
                ''first_names'',
                ''First names'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                5,
                ''f'',
                ''persons''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/company-details'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/company-details'', 5, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''last_name'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''person'',
                ''last_name'',
                ''Last name'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                10,
                ''f'',
                ''persons''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/company-details'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/company-details'', 10, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''screen_name'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''person'',
                ''screen_name'',
                ''Screen name'',
                ''textbox_small'',
                ''string'',
                ''f'',
                15,
                ''f'',
                ''users''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/company-details'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/company-details'', 15, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''email'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''person'',
                ''email'',
                ''Email'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                15,
                ''f'',
                ''parties''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/company-details'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/company-details'', 15, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''screen_name'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''person'',
                ''screen_name'',
                ''Screen name'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                20,
                ''f'',
                ''users''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/company-details'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/company-details'', 20, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''salutation_id'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''person'',
                ''salutation_id'',
                ''Salutation'',
                ''salutation'',
                ''integer'',
                ''f'',
                25,
                ''f'',
                ''persons''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/company-details'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/company-details'', 25, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''cell_phone'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''person'',
                ''cell_phone'',
                ''Cell phone'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                30,
                ''f'',
                ''users_contact''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/company-details'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/company-details'', 30, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''work_phone'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''person'',
                ''work_phone'',
                ''Work phone'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                35,
                ''f'',
                ''users_contact''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/company-details'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/company-details'', 35, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();