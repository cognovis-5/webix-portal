SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.3.1-5.0.1.3.2.sql','');

----
-- Dynfields for CompanyBasicInfoPortlet (taken both from 'im_company' and 'im_office')
----

-- First add new layouts

-- First add new layout for im_offices
create or replace function inline_0 ()
returns integer as '
    declare
            v_count_page_url integer;
    begin
        -- First we check if such page_url does not already exist for that object_type
        select count(*) into v_count_page_url
            from im_dynfield_layout_pages
            where page_url = ''company'' 
            and object_type = ''im_office'' 
            and layout_type = ''table'';
        IF
            v_count_page_url = 0
        THEN
            -- We create layout page if it does not exist
            insert into im_dynfield_layout_pages (page_url, object_type, layout_type) values (''company'', ''im_office'', ''table'');
        END IF;

        return v_count_page_url;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_count_page_url integer;
    begin
        -- First we check if such page_url does not already exist for that object_type
        select count(*) into v_count_page_url
            from im_dynfield_layout_pages
            where page_url = ''company'' 
            and object_type = ''im_company'' 
            and layout_type = ''table'';
        IF
            v_count_page_url = 0
        THEN
            -- We create layout page if it does not exist
            insert into im_dynfield_layout_pages (page_url, object_type, layout_type) values (''company'', ''im_company'', ''table'');
        END IF;

        return v_count_page_url;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

----
-- Dynfields im_company
----

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''company_name'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_company'',
                ''company_name'',
                ''Name'',
                ''textbox_small'',
                ''string'',
                ''f'',
                5,
                ''f'',
                ''im_companies''
            );
        END IF;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''company'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''company'', 5, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''collmex_id'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_company'',
                ''collmex_id'',
                ''Collmex Id'',
                ''textbox_small'',
                ''string'',
                ''f'',
                10,
                ''f'',
                ''im_companies''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''company'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''company'', 10, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();


create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''vat_number'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_company'',
                ''vat_number'',
                ''VAT Number'',
                ''textbox_small'',
                ''string'',
                ''f'',
                15,
                ''f'',
                ''im_companies''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''company'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''company'', 15, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

----
-- Dynfields im_office
----

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''address_line1'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_office'',
                ''address_line1'',
                ''Address'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                5,
                ''f'',
                ''im_offices''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''company'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''company'', 5, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();


create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''address_postal_code'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_office'',
                ''address_postal_code'',
                ''Postal Code'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                10,
                ''f'',
                ''im_offices''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''company'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''company'', 10, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''address_city'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_office'',
                ''address_city'',
                ''City'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                15,
                ''f'',
                ''im_offices''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''company'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''company'', 15, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''address_state'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_office'',
                ''address_state'',
                ''State'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                20,
                ''f'',
                ''im_offices''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''company'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''company'', 20, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();


create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''address_country_code'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_office'',
                ''address_country_code'',
                ''Country'',
                ''country_codes'',
                ''string'',
                ''f'',
                25,
                ''f'',
                ''im_offices''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''company'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''company'', 25, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''phone'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_office'',
                ''phone'',
                ''State'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                30,
                ''f'',
                ''im_offices''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''company'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''company'', 30, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin
        -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.attribute_name = ''fax'';        
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_office'',
                ''fax'',
                ''State'',
                ''textbox_medium'',
                ''string'',
                ''f'',
                35,
                ''f'',
                ''im_offices''
            );
        END IF;

        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''company'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''company'', 35, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();