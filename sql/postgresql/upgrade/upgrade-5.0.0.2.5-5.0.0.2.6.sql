--
--
-- Copyright (c) 2011, cognovís GmbH, Hamburg, Germany
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.
--
-- @author Malte Sussdorff (malte.sussdorff@cognovis.de)
-- @creation-date 2021-02-28
-- @cvs-id $Id$
--

SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.2.5-5.0.0.2.6.sql','');

-- Add column for commments in packages
create or replace function inline_0 ()
returns integer as '
declare
        v_count  integer;
begin
        select count(*) into v_count from user_tab_columns where table_name = ''im_freelance_packages'' and column_name = ''package_comment'';
        if v_count > 0 then return 0;
        else
            alter table im_freelance_packages add column package_comment text default '''';
            return 1;
        end if;
end;' language 'plpgsql';
select inline_0 ();
drop function inline_0 ();