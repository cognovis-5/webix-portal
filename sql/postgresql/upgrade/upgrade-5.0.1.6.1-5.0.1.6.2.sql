SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.6.1-5.0.1.6.2.sql','');


-- Admin Menu
select create_menu ('webix-portal', 'webix_admin', 'Admin', 'a', 140, 'webix_main_menu', '', 'fas fa-toolbox', 't',459);
select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_admin'), 459, 'read');

-- Childs of Admin Menu
-- Dynview Settings
select create_menu ('webix-portal', 'webix_dynview_settings', 'Dynviews', 'dynviews-settings.dynviews-settings', 145, 'webix_admin', '', 'fas fa-gear-complex', 't', 467);
select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_dynview_settings'), 459, 'read');

select create_menu ('webix-portal', 'webix_dynfield_settings', 'Dynfields', 'dynfields-settings.dynfields-settings', 155, 'webix_admin', '', 'fas fa-input-text', 't', 467);
select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_dynfield_settings'), 459, 'read');

select create_menu ('webix-portal', 'webix_dynfield_widgets', 'Widgets', 'dynfields-settings.widgets-settings', 165, 'webix_admin', '', 'fas fa-object-union', 't', 467);
select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_dynfield_widgets'), 459, 'read');

select create_menu ('webix-portal', 'webix_dynfield_layouts', 'Layouts', 'dynfields-settings.dynfields-layouts-settings', 175, 'webix_admin', '', 'fas fa-pager', 't', 467);
select acs_permission__grant_permission((select menu_id from im_menus where label = 'webix_dynfield_layouts'), 459, 'read');