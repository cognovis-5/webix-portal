SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.4.6-5.0.1.4.7.sql','');

-- email dynfields should be always required_p, regardless od subtype
update im_dynfield_type_attribute_map set required_p = 't' where attribute_id = (select da.attribute_id from im_dynfield_attributes da, acs_attributes a where da.acs_attribute_id = a.attribute_id and a.attribute_name = 'email' limit 1);

-- Let freelancers see their skills portlet
DO $$
DECLARE
    portlet_grant_id integer;
BEGIN
    select portlet_id into portlet_grant_id from webix_portlets  where portlet_type_id = 28510 and object_type = 'person' and portlet_status_id = 28550 limit 1;

    IF portlet_grant_id IS NOT NULL THEN
        select acs_permission__grant_permission(portlet_grant_id, 465, 'read');
        select acs_permission__grant_permission(portlet_grant_id, 465, 'write');
    END IF;
END$$;

