SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.2.9-5.0.0.3.0.sql','');


CREATE OR REPLACE FUNCTION im_assignment_quality_report__new(
   p_context_id integer,        -- default null
   p_assignment_id integer,        -- default null
   p_subject_area_id integer,
   p_comment varchar, -- default empty
   p_creation_user integer,     -- default null
   p_creation_ip varchar      -- default null

) RETURNS integer AS $$
DECLARE
        v_report_id               im_assignment_quality_reports.report_id%TYPE;
BEGIN

	-- Check if we have the same reporter rating an assignment for a quality type twice
        select  report_id into v_report_id
        from    im_assignment_quality_reports r, acs_objects o where r.report_id  = o.object_id
        and o.creation_user = p_creation_user
        and o.context_id = p_context_id
        and r.assignment_id = p_assignment_id;

        IF v_report_id is not null THEN return v_report_id; END IF;

        v_report_id := acs_object__new (
                null,              -- object_id
                'im_assignment_quality_report',          -- object_type
                now(),        -- creation_date
                p_creation_user,        -- creation_user
                p_creation_ip,          -- creation_ip
                p_context_id            -- context_id
        );

        insert into im_assignment_quality_reports ( report_id, assignment_id, comment, subject_area_id) 
        values (v_report_id,p_assignment_id, p_comment, p_subject_area_id);

        return v_report_id;
END;
$$ LANGUAGE plpgsql;