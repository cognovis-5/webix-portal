SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.6.1-5.0.0.6.2.sql','');

-- Icons update 
UPDATE im_menus set menu_gif_small = 'fas fa-calendar-week' WHERE label = 'webix_account_info';
UPDATE im_menus set menu_gif_small = 'fas fa-folder-plus' WHERE label = 'webix_new_offer';
UPDATE im_menus set menu_gif_small = 'fas fa-project-diagram' WHERE label = 'webix_projects';
UPDATE im_menus set menu_gif_small = 'fas fa-users-cog' WHERE label = 'webix_project_assignments';
UPDATE im_menus set menu_gif_small = 'fas fa-user-plus' WHERE label = 'webix_project_select_freelancers';
UPDATE im_menus set menu_gif_small = 'fas fa-list' WHERE label = 'webix_project_tasks';


UPDATE im_menus set menu_gif_small = 'fas fa-coins' WHERE label = 'webix_project_financial_overview';
UPDATE im_menus set menu_gif_small = 'fas fa-tasks' WHERE label = 'webix_my_assignments';

