SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.3.4-5.0.0.3.5.sql','');

-- companies
update im_categories set aux_html2 = 'fas fa-industry' where category_id = 10247;
update im_categories set aux_html2 = 'fas fa-language' where category_id = 56;
update im_categories set aux_html2 = 'fas fa-building' where category_id = 57;


update im_categories set aux_html2 = '#008000' where category_id = 46; 
update im_categories set aux_html2 = '#FF0000' where category_id = 48; 
update im_categories set aux_html2 = '#FFA500' where category_id = 41; 

--projects

update im_categories set aux_html2 = 'fas fa-language' where category_id = 2500;
update im_categories set aux_html2 = 'fas fa-language' where category_id in (select child_id from im_category_hierarchy where parent_id = 2500 );
update im_categories set aux_html2 = 'fas fa-project-diagram' where category_id = 2501;
update im_categories set aux_html2 = 'fas fa-project-diagram' where category_id in (select child_id from im_category_hierarchy where parent_id = 2501 );
update im_categories set aux_html2 = 'fas fa-tasks' where category_id = 85;

update im_categories set aux_html2 = '#FFA500' where category_id = 71; 
update im_categories set aux_html2 = '#008000' where category_id = 76;
update im_categories set aux_html2 = '#0080FF' where category_id = 78;
update im_categories set aux_html2 = '#0080FF' where category_id = 79;
update im_categories set aux_html2 = '#0080FF' where category_id = 81;
update im_categories set aux_html2 = '#FF0000' where category_id = 82; 
update im_categories set aux_html2 = '#FF0000' where category_id = 83; 
