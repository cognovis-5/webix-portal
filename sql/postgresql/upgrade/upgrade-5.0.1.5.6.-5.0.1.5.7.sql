SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.5.6-5.0.1.5.7.sql','');

-- Create new category for pivot report
SELECT im_category_new ('15130', 'Webix Pivot Report', 'Intranet Report Type');


-- Creating widget with potential company accounting_contact_id / primary_contact_id

-- First we create widget
-- I'm doing an union with single primary_contact_id here, because in my local environment in lot of cases primary_contact wasn't company member... That resulted in inability to see current person in combobox. We might remove that in future
select im_dynfield_widget__new (
    null,'im_dynfield_widget',now(),null,null,null,
    'company_persons',			-- widget_name
    'Compan Person',		-- pretty_name
    'Company Persons',		-- pretty_plural
    10007,					-- storage_type_id
    'integer',				-- acs_datatype
    'generic_sql',			-- widget
    'integer',				-- sql_datatype
    '{custom {sql { select object_id_two as user_id, im_name_from_id(object_id_two) as user_name from acs_rels r, im_biz_object_members bom where r.rel_id = bom.rel_id and r.rel_type = ''im_company_employee_rel'' and object_id_one = $object_id UNION select c.primary_contact_id as user_id, im_name_from_id(c.primary_contact_id) as_user_name from im_companies c where company_id = $object_id; }} }'
);

-- primary_contact_id
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin

            -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''im_company''
            and a.attribute_name = ''primary_contact_id'';     
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_company'',
                ''primary_contact_id'',
                ''Primary Contact'',
                ''company_persons'',
                ''integer'',
                ''f'',
                45,
                ''f'',
                ''im_companies''
            );            
        END IF;

        update im_dynfield_attributes set widget_name = ''company_persons'' where attribute_id = v_new_attribute_id;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/company'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/company'', 45, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();



-- accounting_contact_id
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin

            -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''im_company''
            and a.attribute_name = ''accounting_contact_id'';     
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''im_company'',
                ''accounting_contact_id'',
                ''Accounting Contact'',
                ''company_persons'',
                ''integer'',
                ''f'',
                50,
                ''f'',
                ''im_companies''
            );            
        END IF;

        update im_dynfield_attributes set widget_name = ''company_persons'' where attribute_id = v_new_attribute_id;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/company'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/company'', 50, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();