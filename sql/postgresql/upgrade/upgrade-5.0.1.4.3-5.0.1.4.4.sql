SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.4.3-5.0.1.4.4.sql','');


create or replace function inline_0 ()
returns integer as $body$
declare
        v_menu_id                 integer;
begin
    delete from im_menus where url = 'invoices.invoices';
    select create_menu ('webix-portal', 'webix_financial_overview', 'Financial Overview', 'invoices.invoices', 120, 'webix_main_menu', '', 'fas fa-coins', 't',461) into v_menu_id;
    -- admins
    perform acs_permission__grant_permission(v_menu_id, 461, 'read');
    -- po's
    perform acs_permission__grant_permission(v_menu_id, 465, 'read');
    return 0;
end;$body$ language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

