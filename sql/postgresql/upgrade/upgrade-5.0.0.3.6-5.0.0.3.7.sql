SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.3.6-5.0.0.3.7.sql','');

alter table im_trans_price_history drop constraint im_trans_price_history_project_fk;
alter table im_trans_price_history add constraint im_trans_price_history_project_fk foreign key (project_id) references im_projects on delete cascade;
