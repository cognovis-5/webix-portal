SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.5.3-5.0.1.5.4.sql','');

-- Create MyProjectsPortlet in account-info page
SELECT im_category_new ('28517', 'ReportPortlet', 'Webix Portlet Type');
SELECT webix_portlet__new (984891, '46.205.146.15', 28517, 28550, 'im_project', 'account-info');

-- Create report columns
CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS INTEGER AS
$$
    DECLARE
            v_new_report_id integer;
            v_report_menu_id integer;
            v_creation_user_id integer;
            v_creation_user_ip text;
    BEGIN
        -- Set values
        SELECT 984891 INTO v_creation_user_id;
        SELECT '46.205.146.15' INTO v_creation_user_ip;

        SELECT im_menu__new (
        	        null,                   -- p_menu_id
        	        'im_menu',              -- object_type
        	        now(),                  -- creation_date
        	        null,                   -- creation_user
        	        null,                   -- creation_ip
        	        null,                   -- context_id
        	        'cognovis-core',          -- package_name
        	        'myprojects',                 -- label
        	        'My Projects',                  -- name
        	        '/intranet-reporting/view',                   -- url
        	        5,	-- sort_order
        	        25978,	-- parent_menu_id
        	        null                    -- p_visible_tcl
        ) INTO v_report_menu_id;

        -- Next step is creating new report
        SELECT im_report_new('My Projects', '', 'webix-portal', 0, v_report_menu_id, 'select project_id, project_nr, project_name, project_type_id, project_status_id from im_projects p where project_lead_id = :rest_user_id and project_status_id in(71,72,76) and project_type_id not in(100) order by project_id desc')
        INTO v_new_report_id;

        -- Now that we have new report, we can create columns
        PERFORM im_report_column__new(v_new_report_id, 'Project Name', 15050, 15156, 0, 'project_id', 'Project name (cognovis object)', v_creation_user_id, v_creation_user_ip);
        PERFORM im_report_column__new(v_new_report_id, 'Project Nr', 15050, 15151, 5, 'project_nr', 'Project number', v_creation_user_id, v_creation_user_ip);
        PERFORM im_report_column__new(v_new_report_id, 'Type', 15050, 15152, 10, 'project_type_id', 'Project type', v_creation_user_id, v_creation_user_ip);
        PERFORM im_report_column__new(v_new_report_id, 'Status', 15050, 15152, 15, 'project_status_id', 'Project status', v_creation_user_id, v_creation_user_ip);

        -- Grant permissions to PMs
        PERFORM acs_permission__grant_permission(v_new_report_id, 467, 'read');
        PERFORM acs_permission__grant_permission(v_new_report_id, 467, 'write');

        return v_new_report_id;
    END;
$$ LANGUAGE 'plpgsql';


SELECT inline_0();
DROP FUNCTION inline_0();
