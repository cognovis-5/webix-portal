SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.2.7-5.0.0.2.8.sql','');


-- Revoke freelancers permissios for Projects and Invoices

select acs_permission__revoke_permission(1311187, 465, 'read');
select acs_permission__revoke_permission(1311183, 465, 'read');

select create_menu ('webix-portal', 'webix_my_assignments', 'My Assignments', 'assignments.my-assignments', 60, 'webix_main_menu', '', 'fas fa-clipboard-list', 't',465);