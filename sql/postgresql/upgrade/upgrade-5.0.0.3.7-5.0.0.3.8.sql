SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.3.7-5.0.0.3.8.sql','');
update im_materials set material_type_id = 9016 where description = 'Automatically generated';

create or replace function inline_0 ()
returns integer as '
declare
        v_count         integer;
begin
        select  count(*) into v_count from im_materials where material_nr = ''surcharge'';
        if v_count = 0 then

        perform im_material__new (
                 (select nextval from acs_object_id_seq)::integer, ''im_material'', now(), null, ''0.0.0.0'', null,
                ''Surcharge'', ''surcharge'', 9000, 9100, 322, ''Surcharge Material for line items indicating a surcharge''
        );
        end if;

        return 0;
end;' language 'plpgsql';
select inline_0 ();
drop function inline_0 ();

create or replace function inline_0 ()
returns integer as '
declare
        v_count         integer;
begin
        select  count(*) into v_count from im_materials where material_nr = ''discount'';
        if v_count = 0 then

        perform im_material__new (
                 (select nextval from acs_object_id_seq)::integer, ''im_material'', now(), null, ''0.0.0.0'', null,
                ''Discount'', ''discount'', 9000, 9100, 322, ''Discount Material for line items indicating a discount''
        );
        end if;

        return 0;
end;' language 'plpgsql';
select inline_0 ();
drop function inline_0 ();