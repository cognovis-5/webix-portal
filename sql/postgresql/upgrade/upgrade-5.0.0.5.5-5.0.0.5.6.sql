SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.5.5-5.0.0.5.6.sql','');

update im_freelance_assignments set assignment_status_id = 4231 
    where assignment_id in (
        select assignment_id 
        from im_freelance_assignments fa, im_freelance_packages fp, im_projects p 
        where fa.freelance_package_id = fp.freelance_package_id and fp.project_id = p.project_id 
        and p.project_status_id in (81,82,83,77,78,79) and assignment_status_id in (4221,4222,4225)
    );