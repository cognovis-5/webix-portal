SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.1.6-5.0.0.1.7.sql','');

-- Get the freelancer status colors
SELECT im_category_new ('4250', 'Main freelancer', 'Intranet Freelancer Selection Status','Main freelancers as set in the customer base data');
update im_categories set sort_order = 0, aux_string1 = 'purple' where category_id = 4250;

SELECT im_category_new ('4251', 'New freelancer', 'Intranet Freelancer Selection Status','A freelancer is considered new until he/she has received three credit notes');
update im_categories set sort_order = 1, aux_string1 = 'turquoise' where category_id = 4251;

SELECT im_category_new ('4252', 'Requested freelancer', 'Intranet Freelancer Selection Status','Has already been requested for the project');
update im_categories set sort_order = 2, aux_string1 = 'yellow' where category_id = 4252;

SELECT im_category_new ('4253', 'Accepted freelancer', 'Intranet Freelancer Selection Status','Has already accepted and is working on an assignment for this project');
update im_categories set sort_order = 3, aux_string1 = 'green' where category_id = 4253;

SELECT im_category_new ('4254', 'Finished freelancer', 'Intranet Freelancer Selection Status','Has already accepted and completed an assignment for this project');
update im_categories set sort_order = 4, aux_string1 = 'blue' where category_id = 4254;

SELECT im_category_new ('4255', 'Rejected freelancer', 'Intranet Freelancer Selection Status','Has rejected an assignment for this project');
update im_categories set sort_order = 5, aux_string1 = 'red' where category_id = 4255;