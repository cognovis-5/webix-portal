-- upgrade-5.0.0.0.6-5.0.0.0.7.sql
SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.0.6-5.0.0.0.7.sql','');


-- Project Info Menu
select create_or_update_menu (2001022, 'webix-portal', 'Info', 'webix_project_info', 'project-info.project-info', 65, 'webix_projects', '', 'fa fa-info', 't');
-- admins
select acs_permission__grant_permission(2001022, 459, 'read');
-- po's
select acs_permission__grant_permission(2001022, 467, 'read');
-- customers
select acs_permission__grant_permission(2001022, 461, 'read');