SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.1.4-5.0.0.1.5.sql','');

-- Create the action buttons for the invoice screen
-- 463 - Employees
-- 467 - PMs
-- 471 - Accounting

select create_menu('webix-portal', 'webix_actions', 'Webix Action Buttons', '', 10, 'top', '', '', 't', 463);
select create_menu('webix-portal', 'webix_action_invoice', 'Webix Invoice Buttons', '', 10, 'webix_actions', '', 'fa fa-file-invoice', 't', 463);

select create_menu('webix-portal', 'invoice_go_to_project', 'Go back to the project', '', 1, 'webix_action_invoice', '', 'fa fa-arrow-left', 't', 463);
select create_menu('webix-portal', 'invoice_download_pdf', 'Download PDF of financial document and mark it as send', '', 2, 'webix_action_invoice', '', 'fa fa-download blue-icon', 't', 463);
select create_menu('webix-portal', 'invoice_preview_pdf', 'Preview a version of the PDF', '', 3, 'webix_action_invoice', '', 'fa fa-eye blue-icon', 't', 463);
select create_menu('webix-portal', 'invoice_preview_word', 'Preview a LibreOffice version for editing', '', 4, 'webix_action_invoice', '', 'fa fa-file-word blue-icon', 't', 463);
select create_menu('webix-portal', 'invoice_convert_to_invoice', 'Convert a quote to an invoice', '', 5, 'webix_action_invoice', '[db_string quote_p "select 1 from im_costs where cost_type_id = [im_cost_type_quote] and cost_id = :object_id" -default 0]', 'fa fa-euro-sign blue-icon', 't', 467);
select create_menu('webix-portal', 'invoice_delete_invoice', 'Delete the financial document (CAREFUL!) and all generated PDFs', '', 6, 'webix_action_invoice', '[im_object_permission -object_id $object_id -user_id $user_id -privilege "admin"]', 'fa fa-trash-alt blue-icon', 't', 471);
select create_menu('webix-portal', 'invoice_mail_invoice', 'Mail the financial document to the recipient', '', 7, 'webix_action_invoice', '', 'fa fa-paper-plane blue-icon', 't', 463);
