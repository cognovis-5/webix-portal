SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.5.8-5.0.1.5.9.sql','');

-- Add new table to possible 'person' tables
insert into acs_object_type_tables values ('person', 'user_preferences', 'user_id');

-- Create user locale widget
select im_dynfield_widget__new (
    null,'im_dynfield_widget',now(),null,null,null,
    'user_locale',			-- widget_name
    'Users Locale',		-- pretty_name
    'Users Locales',		-- pretty_plural
    10007,					-- storage_type_id
    'string',				-- acs_datatype
    'generic_sql',			-- widget
    'varchar(5)',				-- sql_datatype
    '{custom {sql {select distinct locale as id,locale as value from user_preferences where locale is not null; }}}'
);

-- user locale
create or replace function inline_0 ()
returns integer as '
    declare
            v_new_attribute_id integer;
            v_dynfield_layout_count integer;
    begin

            -- First we check if such attribute already exists 
        select d.attribute_id into v_new_attribute_id
            from acs_attributes a, im_dynfield_attributes d
            where a.attribute_id = d.acs_attribute_id
            and a.object_type = ''person''
            and a.attribute_name = ''locale'';     
        IF
            v_new_attribute_id is null
        THEN
            -- First we create dynfield attribute
            v_new_attribute_id := im_dynfield_attribute_new (
                ''person'',
                ''locale'',
                ''User locale'',
                ''user_locale'',
                ''string'',
                ''f'',
                1,
                ''f'',
                ''user_preferences''
            );            
        END IF;

        update im_dynfield_attributes set widget_name = ''user_locale'' where attribute_id = v_new_attribute_id;
        
        -- Now we can connect that to our layout page
        select count(*) from im_dynfield_layout where attribute_id = v_new_attribute_id and page_url = ''/#!/company-details'' into v_dynfield_layout_count;
        IF 
            v_dynfield_layout_count = 0
        THEN
            insert into im_dynfield_layout (attribute_id, page_url, pos_y, label_style) values (v_new_attribute_id, ''/#!/company-details'', 1, ''plain'');
        END IF;

        -- Grant permissions
            -- Customers
            perform acs_permission__grant_permission(v_new_attribute_id, 461, ''write'');
            -- Employees
            perform acs_permission__grant_permission(v_new_attribute_id, 463, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 465, ''write'');
            -- Freelancers
            perform acs_permission__grant_permission(v_new_attribute_id, 467, ''write'');
            -- Accounting
            perform acs_permission__grant_permission(v_new_attribute_id, 471, ''write'');

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();
