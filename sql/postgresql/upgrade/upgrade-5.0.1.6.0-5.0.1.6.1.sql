SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.1.6.0-5.0.1.6.1.sql','');

-- Reset the sequence
SELECT setval('im_view_columns_seq', (SELECT MAX(column_id) + 1 FROM im_view_columns));

CREATE OR REPLACE FUNCTION insert_into_im_views(
    p_view_name varchar,
    p_view_type_id integer,
    p_view_status_id integer,
    p_visible_for varchar,
    p_view_sql varchar,
    p_sort_order integer,
    p_view_label varchar,
    p_sencha_configuration varchar
)
RETURNS integer AS $$
DECLARE
    v_view_id integer;
BEGIN
    -- Check if the view_name already exists
    SELECT view_id INTO v_view_id FROM im_views WHERE view_name = p_view_name;

    IF v_view_id IS NULL THEN
        -- Insert into im_views
        INSERT INTO im_views (view_id, view_name, view_type_id, view_status_id, visible_for, view_sql, sort_order, view_label, sencha_configuration)
        VALUES (nextval('im_views_seq'),p_view_name, p_view_type_id, p_view_status_id, p_visible_for, p_view_sql, p_sort_order, p_view_label, p_sencha_configuration);
        SELECT view_id INTO v_view_id FROM im_views WHERE view_name = p_view_name;
    END IF;
    RETURN v_view_id;
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION insert_into_im_view_columns(
    p_view_id integer,
    p_group_id integer,
    p_column_name varchar,
    p_column_render_tcl varchar,
    p_extra_select varchar,
    p_extra_from varchar,
    p_extra_where varchar,
    p_sort_order integer,
    p_order_by_clause varchar,
    p_visible_for varchar,
    p_ajax_configuration text,
    p_variable_name varchar,
    p_datatype varchar
)
RETURNS void AS $$
BEGIN
    -- Check if the column_name and variable_name already exists
    IF NOT EXISTS (SELECT 1 FROM im_view_columns WHERE column_name = p_column_name AND view_id = p_view_id AND variable_name = p_variable_name) THEN
        -- Insert into im_view_columns
        INSERT INTO im_view_columns (column_id, view_id, group_id, column_name, column_render_tcl, extra_select, extra_from, extra_where, 
                                     sort_order, order_by_clause, visible_for, ajax_configuration, variable_name, datatype)
        VALUES (nextval('im_view_columns_seq'), p_view_id, p_group_id, p_column_name, p_column_render_tcl, p_extra_select, p_extra_from, p_extra_where,
                p_sort_order, p_order_by_clause, p_visible_for, p_ajax_configuration, p_variable_name, p_datatype);
    END IF;
END;
$$ LANGUAGE plpgsql;


-- Current Projects
CREATE OR REPLACE FUNCTION inline_0()

RETURNS void AS $$
DECLARE
    v_new_view_id integer;
BEGIN


    v_new_view_id := insert_into_im_views('webix_current_projects',1420, null , null, 'select p.project_id as object_id, p.project_name, p.project_nr, p.project_type_id as project_type_id, im_name_from_id(p.project_type_id) as project_type_name, p.project_status_id, p.company_id, im_name_from_id(p.project_status_id) as project_status_name, p.source_language_id, im_name_from_id(p.source_language_id) as source_language_name, p.project_lead_id, p.project_lead_id as project_manager_id, p.company_project_nr, p.company_contact_id, im_name_from_id(p.company_contact_id) as company_contact_name, p.cost_quotes_cache, p.end_date, p.contact_again_date, array_to_string(array(SELECT language_id FROM im_target_languages langs WHERE p.project_id = langs.project_id ), '', '') AS target_language_names FROM im_projects p WHERE project_status_id = 76 AND project_type_id not in(100,101) order by project_id desc', 10, 'webix-portal.Current_Projects', null);

    RAISE NOTICE 'New or Existing View ID: %', v_new_view_id;
    
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'object_id', null, null, null, null, 100, null, null, '{"columnType":"idColumn"}', 'object_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_Type', null, null, null, null, 120, null, null, '{"columnType":"iconColumn"}', 'project_type_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_Status', null, null, null, null, 130, null, null, '{"columnType":"selectColumn", "hidden":"true"}', 'project_status_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_name', null, null, null, null, 140, null, null, '{"columnType":"textColumn"}', 'project_name', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Customer', null, null, null, null, 150, null, null, '{"columnType":"textColumn"}', 'company_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_nr', null, null, null, null, 160, null, null, '{"columnType":"selectColumn"}', 'project_nr', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Source_language', null, null, null, null, 170, null, null, '{"columnType":"categoryColumn", "categoryType":"Intranet Translation Language"}', 'source_language_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Target_language', null, null, null, null, 180, null, null, '{"columnType":"categoryColumn", "categoryType":"Intranet Translation Language"}', 'target_language_names', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Manager', null, null, null, null, 190, null, null, '{"columnType":"pmColumn", "width":"40"}', 'project_manager_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Requested_by', null, null, null, null, 200, null, null, '{"columnType":"selectColumn"}', 'company_contact_name', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Price', null, null, null, null, 210, null, null, '{"columnType":"priceColumn"}', 'cost_quotes_cache', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Deadline', null, null, null, null, 210, null, null, '{"columnType":"dateColumn"}', 'end_date', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.contact_again', null, null, null, null, 210, null, null, '{"columnType":"dateColumn"}', 'contact_again_date', 'string');

END;
$$ LANGUAGE plpgsql;

SELECT inline_0();
drop function inline_0();


-- Potential Projects
CREATE OR REPLACE FUNCTION inline_0()

RETURNS void AS $$
DECLARE
    v_new_view_id integer;
BEGIN


    v_new_view_id := insert_into_im_views('webix_potential_projects',1420, null , null, 'SELECT p.project_id as object_id, p.project_name, p.project_nr, p.project_type_id as project_type_id, im_name_from_id(p.project_type_id) as project_type_name, p.project_status_id, p.project_status_id as project_status_id_second, p.company_id, im_name_from_id(p.project_status_id) as project_status_name, p.source_language_id, im_name_from_id(p.source_language_id) as source_language_name, p.project_lead_id, p.project_lead_id as project_manager_id, p.company_project_nr, p.company_contact_id, im_name_from_id(p.company_contact_id) as company_contact_name, p.cost_quotes_cache, p.end_date, p.contact_again_date, array_to_string(array(SELECT language_id FROM im_target_languages langs WHERE p.project_id = langs.project_id ), '', '') AS target_language_names FROM im_projects p WHERE project_status_id = 71 AND project_status_id != 72 AND project_type_id not in(100,101) order by project_id desc', 10, 'webix-portal.Potential_Projects', null);

    RAISE NOTICE 'New or Existing View ID: %', v_new_view_id;
    
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'object_id', null, null, null, null, 100, null, null, '{"columnType":"idColumn"}', 'object_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_Type', null, null, null, null, 120, null, null, '{"columnType":"iconColumn"}', 'project_type_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'Status', null, null, null, null, 130, null, null, '{"columnType":"selectColumn", "hidden":"true"}', 'project_status_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_name', null, null, null, null, 140, null, null, '{"columnType":"textColumn"}', 'project_name', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Customer', null, null, null, null, 150, null, null, '{"columnType":"textColumn"}', 'company_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_nr', null, null, null, null, 160, null, null, '{"columnType":"selectColumn"}', 'project_nr', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Source_language', null, null, null, null, 170, null, null, '{"columnType":"categoryColumn", "categoryType":"Intranet Translation Language"}', 'source_language_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Target_language', null, null, null, null, 180, null, null, '{"columnType":"categoryColumn", "categoryType":"Intranet Translation Language"}', 'target_language_names', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Manager', null, null, null, null, 190, null, null, '{"columnType":"pmColumn", "width":"40"}', 'project_manager_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Requested_by', null, null, null, null, 200, null, null, '{"columnType":"selectColumn"}', 'company_contact_name', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Price', null, null, null, null, 210, null, null, '{"columnType":"priceColumn"}', 'cost_quotes_cache', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Deadline', null, null, null, null, 220, null, null, '{"columnType":"dateColumn"}', 'end_date', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.contact_again', null, null, null, null, 230, null, null, '{"columnType":"dateColumn"}', 'contact_again_date', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_Status', null, null, null, null, 240, null, null, '{"columnType":"selectColumn"}', 'project_status_id_second', 'integer');

END;
$$ LANGUAGE plpgsql;

SELECT inline_0();
drop function inline_0();


-- Requested Projects
CREATE OR REPLACE FUNCTION inline_0()

RETURNS void AS $$
DECLARE
    v_new_view_id integer;
BEGIN


    v_new_view_id := insert_into_im_views('webix_requested_projects',1420, null , null, 'SELECT p.project_id as object_id, p.project_name, p.project_nr, p.project_type_id as project_type_id, im_name_from_id(p.project_type_id) as project_type_name, p.project_status_id, p.company_id, im_name_from_id(p.project_status_id) as project_status_name, p.source_language_id, im_name_from_id(p.source_language_id) as source_language_name, p.project_lead_id, p.project_lead_id as project_manager_id, p.company_project_nr, p.company_contact_id, im_name_from_id(p.company_contact_id) as company_contact_name, p.cost_quotes_cache, p.end_date, p.contact_again_date, array_to_string(array(SELECT language_id FROM im_target_languages langs WHERE p.project_id = langs.project_id ), '', '') AS target_language_name FROM im_projects p WHERE project_status_id = 72 AND project_type_id not in(100,101) order by project_id desc', 10, 'webix-portal.New_requestes_for_quote', null);

    RAISE NOTICE 'New or Existing View ID: %', v_new_view_id;
    
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'object_id', null, null, null, null, 100, null, null, '{"columnType":"idColumn"}', 'object_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_Type', null, null, null, null, 120, null, null, '{"columnType":"iconColumn"}', 'project_type_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_Status', null, null, null, null, 130, null, null, '{"columnType":"selectColumn", "hidden":"true"}', 'project_status_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_name', null, null, null, null, 140, null, null, '{"columnType":"textColumn"}', 'project_name', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Customer', null, null, null, null, 150, null, null, '{"columnType":"textColumn"}', 'company_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_nr', null, null, null, null, 160, null, null, '{"columnType":"selectColumn"}', 'project_nr', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Target_language', null, null, null, null, 180, null, null, '{"columnType":"categoryColumn", "categoryType":"Intranet Translation Language"}', 'target_language_names', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Manager', null, null, null, null, 190, null, null, '{"columnType":"pmColumn", "width":"40"}', 'project_manager_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Requested_by', null, null, null, null, 200, null, null, '{"columnType":"selectColumn"}', 'company_contact_name', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Price', null, null, null, null, 210, null, null, '{"columnType":"priceColumn"}', 'cost_quotes_cache', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Deadline', null, null, null, null, 220, null, null, '{"columnType":"dateColumn"}', 'end_date', 'string');

END;
$$ LANGUAGE plpgsql;

SELECT inline_0();
drop function inline_0();

-- Delivered Projects
CREATE OR REPLACE FUNCTION inline_0()

RETURNS void AS $$
DECLARE
    v_new_view_id integer;
BEGIN


    v_new_view_id := insert_into_im_views('webix_delivered',1420, null , null, 'SELECT p.project_id as object_id, p.project_name, p.project_nr, p.project_type_id as project_type_id, im_name_from_id(p.project_type_id) as project_type_name, p.project_status_id, p.company_id, im_name_from_id(p.project_status_id) as project_status_name, p.source_language_id, im_name_from_id(p.source_language_id) as source_language_name, p.project_lead_id, p.project_lead_id as project_manager_id, p.company_project_nr, p.company_contact_id, im_name_from_id(p.company_contact_id) as company_contact_name, p.cost_quotes_cache, p.end_date, p.contact_again_date, array_to_string(array(SELECT language_id FROM im_target_languages langs WHERE p.project_id = langs.project_id ), '', '') AS target_language_names FROM im_projects p WHERE project_status_id = 78 AND project_type_id not in(100,101) order by project_id desc', 10, 'webix-portal.Delivered_Projects', null);

    RAISE NOTICE 'New or Existing View ID: %', v_new_view_id;
    
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'object_id', null, null, null, null, 100, null, null, '{"columnType":"idColumn"}', 'object_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_Type', null, null, null, null, 120, null, null, '{"columnType":"iconColumn"}', 'project_type_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_Status', null, null, null, null, 130, null, null, '{"columnType":"selectColumn", "hidden":"true"}', 'project_status_id', 'integer');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_name', null, null, null, null, 140, null, null, '{"columnType":"textColumn"}', 'project_name', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Project_nr', null, null, null, null, 160, null, null, '{"columnType":"selectColumn"}', 'project_nr', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Target_language', null, null, null, null, 180, null, null, '{"columnType":"categoryColumn", "categoryType":"Intranet Translation Language"}', 'target_language_names', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Requested_by', null, null, null, null, 200, null, null, '{"columnType":"selectColumn"}', 'company_contact_name', 'string');
    PERFORM insert_into_im_view_columns(v_new_view_id, null, 'webix-portal.Price', null, null, null, null, 210, null, null, '{"columnType":"priceColumn"}', 'cost_quotes_cache', 'string');

END;
$$ LANGUAGE plpgsql;

SELECT inline_0();
drop function inline_0();