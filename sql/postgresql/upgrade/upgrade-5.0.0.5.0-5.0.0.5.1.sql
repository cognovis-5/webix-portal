SELECT acs_log__debug('/packages/webix-portal/sql/postgresql/upgrade/upgrade-5.0.0.5.0-5.0.0.5.1.sql','');

create or replace function inline_0 ()
returns integer as '
declare
        v_count integer;
begin
        select count(*) into v_count from user_tab_columns
        where lower(table_name) = ''im_freelance_packages'' and lower(column_name) = ''target_language_id'';
        IF v_count > 0 THEN return 0; END IF;

        alter table im_freelance_packages add column target_language_id integer references im_categories;

        return 0;
end;' language 'plpgsql';
SELECT inline_0();
DROP FUNCTION inline_0();

create or replace function im_freelance_package__new (
    integer, varchar, integer, integer, integer,varchar, integer
) returns integer as '
DECLARE
   p_creation_user        alias for $1;
   p_creation_ip        alias for $2;
   p_package_type_id            alias for $3;
   p_package_status_id  alias for $4;
   p_project_id         alias for $5;
   p_freelance_package_name       alias for $6;
   p_target_language_id       alias for $7;


    v_freelance_package_id      integer;
BEGIN
    v_freelance_package_id := acs_object__new (
        null,
        ''im_freelance_package'',
    now(),
        p_creation_user,
        p_creation_ip,
        p_project_id
    );

    insert into im_freelance_packages (
        freelance_package_id, project_id, package_type_id, 
        package_status_id, freelance_package_name, target_language_id
    ) values (
        v_freelance_package_id, p_project_id, p_package_type_id, 
        p_package_status_id,p_freelance_package_name, p_target_language_id
    );

    return v_freelance_package_id;
end;' language 'plpgsql';

create or replace function im_freelance_package_file__delete (integer) returns integer as '
DECLARE
    v_freelance_package_file_id      alias for $1;
BEGIN

    -- Erase the im_freelance_packages item associated with the id
    delete from     im_freelance_package_files
    where          freelance_package_file_id = v_freelance_package_file_id;

    -- Erase all the priviledges
    delete from     acs_permissions
    where          object_id = v_freelance_package_file_id;

PERFORM acs_object__delete(v_freelance_package_file_id);

    return 0;
end;' language 'plpgsql';