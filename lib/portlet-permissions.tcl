#
# Copyright (C) 2004 - 2009 ]project-open[
# The code is based on ArsDigita ACS 3.4
#
# This program is free software. You can redistribute it
# and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation;
# either version 2 of the License, or (at your option)
# any later version. This program is distributed in the
# hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

ad_page_contract {
    Show the permissions for all menus in the system

    @author frank.bergmann@project-open.com
} {
    acs_object_type:optional
    nomaster_p:optional
    portlet_id
    { return_url "" }
}

# ------------------------------------------------------
# Defaults & Security
# ------------------------------------------------------

set user_id [auth::require_login]
set user_is_admin_p [im_is_user_site_wide_or_intranet_admin $user_id]

# If used as 
if {![info exists nomaster_p]} { set nomaster_p 0 }
if {![info exists acs_object_type]} { set acs_object_type "" }

if {!$user_is_admin_p} {
    ad_return_complaint 1 "You have insufficient privileges to use this page"
    return
}

if {"" == $return_url} { set return_url [im_url_with_query] }

set toggle_url "/intranet/admin/toggle"
set group_url "/admin/groups/one"

set bgcolor(0) " class=rowodd"
set bgcolor(1) " class=roweven"

# ------------------------------------------------------
# Get the list of all dynfields
# and generate the dynamic part of the SQL
# ------------------------------------------------------

set table_header "
<tr>
  <th>Portlet</th>
\n"


set group_list_sql "
	select DISTINCT
	        g.group_name,
	        g.group_id,
	        p.profile_gif
	from	acs_objects o,
	        groups g,
	        im_profiles p
	where	g.group_id = o.object_id
	        and g.group_id = p.profile_id
	        and o.object_type = 'im_profile'
		and g.group_id != [im_profile_po_admins]
"

set main_sql_select ""
set num_groups 0
set object_type_constraint "1 = 1"

set group_ids [list]
set group_names [list]

db_foreach group_list $group_list_sql {
  lappend group_ids $group_id
  lappend group_names $group_name

  set p${group_id}_read_p [permission::permission_p -object_id $portlet_id -party_id $group_id -privilege "read"]
  set p${group_id}_write_p [permission::permission_p -object_id $portlet_id -party_id $group_id -privilege "write"]
  append table_header "
      <th><A href=$group_url?group_id=$group_id>
      [im_gif -translate_p 1 $profile_gif $group_name]
    </A></th>\n"
    incr num_groups
}
append table_header "
</tr>
"


# ------------------------------------------------------
# Main SQL: Extract the permissions for all Dynfields
# ------------------------------------------------------

set table "
<table id='editable_table'>
$table_header\n"

set object_type_where ""
if {"" != $acs_object_type} { set object_type_where "and aa.object_type = :acs_object_type" }

set ctr 0
incr ctr
append table "\n<tr$bgcolor([expr {$ctr % 2}])>\n"
append table "
  <td>
    [_ webix-portal.portlet_$portlet_id]
  </td>
"

foreach horiz_group_id $group_ids {
  set read_p [set p${horiz_group_id}_read_p]
  set write_p [set p${horiz_group_id}_write_p]
	set object_id $portlet_id

	set action "add_readable"
	set letter "r"
  if {$read_p} {
    set action "remove_readable"
    set letter "<b>R</b>"
  }
	set read "<A href=[export_vars -base $toggle_url { horiz_group_id object_id action return_url}]>$letter</A>"

	set action "add_writable"
	set letter "w"
  if {$write_p} {
	    set action "remove_writable"
     set letter "<b>W</b>"
  }
	set write "<A href=[export_vars -base $toggle_url { horiz_group_id object_id action return_url}]>$letter</A>"
	append table "<td align=center>$read $write</td>"
}
append table "
</tr>
</table>
</form>
"