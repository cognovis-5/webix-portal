<h1>@page_title;noquote@</h1>

<b>Explanation</b>
<ul>
<li>N - D 
<li>First = "None" - Don't display the field
<li>Second = "Display" - "Read only" mode
</ul>


<form action="/webix-portal/portlet-type-map-2" method=POST>
<%= [export_vars -form {acs_object_type object_subtype_id return_url}] %>

<table>
@header_html;noquote@
@body_html;noquote@
<tr>
  <td></td>
  <td colspan="99"><input type="submit"></td>
</tr>
</table>

</form>


