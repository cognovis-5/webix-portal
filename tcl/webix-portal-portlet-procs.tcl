ad_library {
    Handling of portlets for webix
    @author malte.sussdorff@cognovis.de
}

ad_proc webix_portlet_status_enabled {} {} {return 28550}
ad_proc webix_portlet_status_disabled {} {} {return 28551}

namespace eval webix::portlet {

    ad_proc -public create {
        { -user_id "" }
        -portlet_type_id:required
        -object_type:required
        -page_url:required
        { -portlet_status_id 28550 }
        { -sort_order ""}
        { -location "left"}
        { -portlet_name ""}
    } {
        Adds a portlet for webix to use

        @param portlet_type_id Type of portlet - defines the Webix Class to use for the portlet
        @param object_type Object Type for which this portlet is used. Keep in mind the same portlet_type_id can be used for different object_types
        @param page_url On which page is this portlet shown
        @param portlet_status_id Whats the status (defaults to enabled)
    } {
        set portlet_id [db_string exists_p "select portlet_id from webix_portlets where object_type = :object_type and page_url = :page_url and portlet_type_id = :portlet_type_id" -default ""]
        if {$user_id eq ""} { set user_id [auth::get_user_id]}

        if {$portlet_id eq ""} {
            set portlet_id [db_string create_portlet "select webix_portlet__new(:user_id,'',:portlet_type_id,:portlet_status_id,:object_type,:page_url)"]
        }

        if {$sort_order eq ""} {
            set sort_order "select max(sort_order)+1 from webix_portlets where object_type = :object_type and page_url = :page_url"
        }

        db_dml update_portlet "update webix_portlets set sort_order = :sort_order, location = :location where portlet_id = :portlet_id"

        # Use I18N to store the portlet name by default
        if {$portlet_name ne ""} {
            lang::message::register en_US "webix-portal" "portlet_$portlet_id" $portlet_name
        }
        return $portlet_id
    }
}

namespace eval cog_rest::json_object {
    ad_proc -public webix_portlet {} {
        @return portlet_id integer ID for the portlet
        @return object_type string For which object type is this portlet
        @return portlet_type category "Webix Portlet Type" Type of the portlet (usually the category is the class name)
        @return portlet_status category "Webix Portlet Status" Status for this portlet - usually enabled / disabled
        @return page_url string On which URL should we show this portlet
        @return sort_order integer For sorting the portlets by default.
        @return portlet_name string Default name from I18N
        @return location string where to put the portlet
    } -
}

ad_proc -public cog_rest::get::webix_portlets {
    -rest_user_id:required
    -object_id
    {-object_type ""}
    -portlet_status_id 
    -page_url
} {
    Returns webix portlets the user can see

    @param object_id integer Object we are looking at to get the portlets back
    @param object_type string Object type to look at (in case the object_id is missing). Would display all of the portlets regardless of subtype configuration
    @param portlet_status_id category "Webix Portlet Status" Should usually be provided with 28550 to only display enabled portlets
    @param page_url string On which page are we displaying the portlet.

    @return portlets json_array webix_portlet Array of portlets
} {
    set portlets [list]
    set where_clause_list [list]
    set object_subtype_ids [list]

    if {[info exists object_id]} {
        set object_type [acs_object_type $object_id]

        switch $object_type {
            person - user - party {
                set object_subtype_ids [db_list members "select c.category_id from group_member_map g, im_categories c where 
                    g.member_id = :object_id 
                    and g.group_id = c.aux_int1
                    and c.category_type = 'Intranet User Type'
                    and g.member_id not in (
                    -- Exclude banned or deleted users
                    select	m.member_id
                    from	group_member_map m,
                        membership_rels mr
                    where	m.rel_id = mr.rel_id and
                        m.group_id = acs__magic_object_id('registered_users') and
                        m.container_id = m.group_id and
                        mr.member_state != 'approved')"]
            }
            default {
                db_1row object_type_info "select table_name, id_column, type_column from acs_object_types where object_type = :object_type"
                set object_subtype_id ""
                if {$table_name ne "" && $id_column ne "" && $type_column ne ""} {
                    set object_subtype_id [db_string subtype "select $type_column from $table_name where $id_column = :object_id" -default ""]
                }
                if {$object_subtype_id ne ""} {
                    lappend object_subtype_ids $object_subtype_id
                }
            }
        }
    }
    switch $object_type {
        person - user - party {
            set object_type "person"
        }
    }

    if {[llength $object_subtype_ids]>0} {
        lappend where_clause_list "portlet_id in (select portlet_id from webix_portlet_object_type_map where object_type_id in ([template::util::tcl_to_sql_list $object_subtype_ids]))"
    }

    if {[exists_and_not_null object_type]} {
        lappend where_clause_list "object_type = :object_type"
    }

    if {[exists_and_not_null portlet_status_id]} {
        lappend where_clause_list "portlet_status_id = :portlet_status_id"
    }

    if {[exists_and_not_null page_url]} {
        lappend where_clause_list "page_url = :page_url"
    }

    if {[llength $where_clause_list] == 0} {
        lappend where_clause_list "portlet_status_id = 28550"
    }

    db_foreach portlet "select portlet_id, object_type, portlet_type_id,portlet_status_id, 
        page_url, sort_order, location, im_object_permission_p(portlet_id,:rest_user_id,'read') as permission_p
        from webix_portlets where [join $where_clause_list " and "]" {
        
        if {$permission_p} {
            set portlet_name [cog::message::lookup -user_id $rest_user_id -key "webix-portal.portlet_$portlet_id"]
            
            lappend portlets [cog_rest::json_object]
        }
    }
    return [cog_rest::json_response]
}