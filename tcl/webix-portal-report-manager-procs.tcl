ad_library {
    Rest Procedures for posting the webix-portal report manager complex widget
    @author malte.sussdorff@cognovis.de
}

namespace eval cog_rest::json_object {
   
    ad_proc dynfield_attribute {} {
        @return attribute_name string ID of the attribute
        @return pretty_name string Display name
        @return filter boolean Can you filter by this attribute
        @return edit boolean Are you allowed to edit this attribute
        @return type string number / text / ....
        @return ref string object type of the referencing object (e.g. companies for company_id in projects)

    } - 
    
    ad_proc dynfield_ref {} {
        @return id number
        @return target string Name of the dynfield_object_type offering to be the target of a reference
        @return source string Name of the source object_type (where we reference from)
        @return name string Name of dynfield attribute in the SOURCE object_type
    } -

    ad_proc dynfield_object_type {} {
        @return pretty_name string Pretty name of the object type
        @return object_type string Actual object type name / id
        @return dynfield_attributes json_array dynfield_attribute Array of dynfield_attributes
        @return dynfield_refs json_array dynfield_ref Array of dynfield_refs with all the refs where the object_type is either source or target.
    } - 

    ad_proc report_module {} {
        @return report_module_id number ID of the report
        @return config string Configuration of the report
        @return report_name string Name of the Report
        @return updated timestamp When was it last updated
    } -

    ad_proc report_module_body {} {
        @param config string Configuration of the report
        @param report_name string Name of the Report
    } - 

}

ad_proc -public cog_rest::get::report_object_types {
    -rest_user_id:required
} {
    Returns the "objects" definition for the report manager

    @return objects json_array dynfield_object_type
} {
    set objects [list]

    # Foreach object type .....
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::report_modules {
    -rest_user_id:required
    { -report_module_id "" }
} {
    Returns the "modules" for the report manager. Literally a list of reports and their configuration text

    @return modules json_array report_module Array of modules
} {
    set modules [list]
    set where_clause_list [list]
    if {$report_module_id ne ""} {
        lappend where_clause_list "report_module_id = :report_module_id"
    } else {
        lappend where_clause_list "1=1"
    }
    
    db_foreach module "select report_name, report_module_id, config, coalesce(last_modified,creation_date) as updated from webix_report_modules where [join $where_clause_list " and "]" {
        lappend modules [cog_rest::json_object]
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::report_module {
    -rest_user_id:required
    -report_name:required
    -config:required
} {
    @param report_module_body request_body Name/Config information of the module

    @return module json_object report_module Single Report module
} {

    set report_module_id [webix::report::new -user_id $rest_user_id -config $config -report_name $report_name]
    set report [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::report_modules -rest_user_id $rest_user_id -report_module_id $report_module_id]]

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::report_module {
    -rest_user_id:required
    -report_name:required
    -config:required
    -report_module_id:required
} {
    Updates a report module

    @param report_module_id number ID of the report
    @param report_module_body request_body Name/Config information of the module

    @return module json_object report_module Single Report module
} {

    webix::report::update -user_id $rest_user_id -config $config -report_module_id $report_module_id -report_name $report_name
    set report [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::report_modules -rest_user_id $rest_user_id -report_module_id $report_module_id]]

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::report_module {
    -rest_user_id:required
    -report_module_id:required
} {
    Delete a module
    @param report_module_id number ID of the report    
} {
    set success [webix::report::delete -user_id $rest_user_id -report_module_id $report_module_id] 
    if {$success ne 1} {
        cog_rest::error -http_status 400 -message "$success"
    }
    return 1
}

namespace eval webix::report {
    ad_proc -public new {
        -report_name:required
        -config:required
        { -report_status_id ""}
        { -report_type_id "" }
        { -user_id "" }
    } {
        Adds a new report from the report manager

        @param report_name Name of the report
        @param config Configuration string of webix.

        @return report_module_id ID of the module just created
    } {
        if {$user_id eq ""} { set user_id [auth::get_user_id]}
        set report_module_id [db_string create_portlet "select webix_report_module__new(:user_id,'',:report_type_id,:report_status_id,:report_name,:config)"]
        return $report_module_id
    }

    ad_proc -public delete {
        -report_module_id:required
        { -user_id ""}
    } {
        Delete a module
    } {
        if {$user_id eq ""} { set user_id [auth::get_user_id]}
        set success [db_string delete_report "select webix_report_module__delete(:report_module_id) from dual" -default 0]
        return $success
    }

    ad_proc -public update {
        {-user_id ""}
        -report_module_id:required
        -report_name:required
        -config:required
    } {
        Update a report module

        @param report_module_id ID of the  module
        @param report_name Name of the report
        @param config Configuration string of webix.

    } {
        if {$user_id eq ""} { set user_id [auth::get_user_id]}
        db_dml update_module "update webix_report_modules set report_name = :report_name, config = :config where report_module_id = :report_module_id"
        db_dml update_object "update acs_objects set last_modified=now(), modifying_user = :user_id where object_id = :report_module_id"
        return $report_module_id
    }
}