ad_library {
    Rest Procedures for handling system specific stuff with webix-core
    @author malte.sussdorff@cognovis.de
}

ad_proc -public cog_rest::put::forgot_password_url {
    { -email ""}
    { -username ""}
} {
    Start the whole process of password reset

    @param email string Email of the user who lost his password
    @param username string Username of the user who lost his password

    @return success boolean true if sending a token was a success

} {
    if {$email ne ""} {
        set user_id [db_string user_id_from_email "SELECT user_id from cc_users where email =:email" -default ""]
        if {$user_id eq ""} {
            cog_rest::error -http_status 401 -message "[_ webix-portal.err_no_user_linked_email]"
        }
    } 

    if {$username ne "" && $user_id eq ""} {
        set user_id [db_string user_id_from_username "SELECT user_id from cc_users where username =:username" -default ""]
        if {$user_id eq ""} {
            cog_rest::error -http_status 401 -message "<err_no_user_linked_id No user_id linked to username %username%#>"
        }
    }

    if {$user_id ne ""} {
        db_1row other_user_info "select first_names, last_name, username, email from cc_users where user_id =:user_id"
        set token [im_generate_auto_login -user_id $user_id]
        set bearer_token [base64::encode "${user_id}:$token"]

        set portal_url [parameter::get_from_package_key -package_key "webix-portal" -parameter "WebixPortalUrl"]
        set forgot_password_endpoint [parameter::get_from_package_key -package_key "webix-portal" -parameter "WebixPortalForgotPasswordUrl" -default "forgot-password"]

        set forgotten_password_url [export_vars -no_base_encode -base "$portal_url/#!/$forgot_password_endpoint" -url {{t $bearer_token}}]

        set locale [lang::user::locale -user_id $user_id]
        set subject "[lang::message::lookup $locale webix-portal.change_password_email_subject]"
        set body "[lang::message::lookup $locale webix-portal.change_password_email_body]"
        
        set message_id [intranet_chilkat::send_mail -to_party_ids $user_id -subject $subject -body $body -no_callback -from_addr [ad_system_owner]]
        if {$message_id eq ""} {
            cog_rest::error -http_status 401 -message "[_ webix-portal.err_could_not_send_mail]"
        }
    } else {
        cog_rest::error -http_status 401 -message "[_ webix-portal.err_no_user_id]"
    }
    return "{\"success\": true}"
}
