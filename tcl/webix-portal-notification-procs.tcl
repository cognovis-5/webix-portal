ad_library {
    Handling of notifications system for webix
    @author malte.sussdorff@cognovis.de
}

ad_proc webix_notification_type_assignment_overdue {} {} {return 4260}
ad_proc webix_notification_type_assignment_delivered {} {} {return 4261}
ad_proc webix_notification_type_assignment_accepted {} {} {return 4262}
ad_proc webix_notification_type_assignment_denied {} {} {return 4263}
ad_proc webix_notification_type_assignment_request_out {} {} {return 4264}
ad_proc webix_notification_type_assignment_deadline_change {} {} {return 4271}
ad_proc webix_notification_type_project_overdue {} {} {return 4265}
ad_proc webix_notification_type_quote_accepted {} {} {return 4266}
ad_proc webix_notification_type_quote_denied {} {} {return 4267}
ad_proc webix_notification_type_project_delivered {} {} {return 4268}
ad_proc webix_notification_type_project_contact_again {} {} {return 4269}
ad_proc webix_notification_type_package_file_missing {} {} {return 4270}
ad_proc webix_notification_type_file_uploaded {} {} {return 4272}
ad_proc webix_notification_type_note_added {} {} {return 4273}
ad_proc webix_notification_type_default {} {} {return 4280}

ad_proc webix_notification_status_default {} {} {return 4240}
ad_proc webix_notification_status_warning {} {} {return 4241}
ad_proc webix_notification_status_important {} {} {return 4242}


ad_proc webix_notification_action_type_mark_read {} {} {return 14300}
ad_proc webix_notification_action_type_upload_files {} {} {return 14301}
ad_proc webix_notification_action_type_send_email {} {} {return 14302}
ad_proc webix_notification_action_type_open_object {} {} {return 14303}
ad_proc webix_notification_action_type_open_company {} {} {return 14304}
ad_proc webix_notification_action_type_open_contact {} {} {return 14305}
ad_proc webix_notification_action_type_freelancer_selection {} {} {return 14306}
ad_proc webix_notification_action_type_download_files {} {} {return 14307}
ad_proc webix_notification_action_type_cr_direct_download {} {} {return 14308}
ad_proc webix_notification_action_type_open_assignment_overview {} {} {return 14309}
ad_proc webix_notification_action_type_view_comments {} {} {return 14310}

namespace eval cog_rest::json_object {
    ad_proc -public webix_notification {} {
        @return notification object webix_notification The notification object, especially the ID is of note here
        @return recipient object person Who is receiving the notification
        @return notification_type category "Webix Notification Type" Type of notification we are looking at
        @return notification_status category "Webix Notification Status" Status of the notification we are looking at
        @return notification_color string Color of the notification. By default derived from the status
        @return viewed_date date-time When was the notification viewed (if at all)
        @return message string Text we are displaying for the notification. Default comes form the notification_type
        @return context json_object cognovis_object Object which caused the notification (context)
        @return project json_object cognovis_object Project in which the notification occured
        @return project_nr string ProjectNr of the project (as the name usually is too long)
        @return actions json_array webix_notification_action Actions for  the notification
    } -

    ad_proc -public webix_notification_body {} {
        @param notification_type_id category "Webix Notification Type" Type of notification we are looking at
        @param notification_status_id category "Webix Notification Status" Status of the notification we are looking at
        @param message string Text we are displaying for the notification. Default comes form the notification_type
        @param context_id integer Object which caused the notification (context)
        @param project_id object im_project Project in which the notification is created
        @param recipient_id integer Recipient of the notification
    } -

    ad_proc -public webix_notification_action {} {
        @return action_type category "Webix Notification Action Type" Type of action we can perform
        @return action_text string Text we want to pass through to the action (e.g. the default text for an email)
        @return action_help string Tooltip text to describe the action based of the action_type
        @return action_icon string Class to use for the icon
        @return action_object json_object cognovis_object Object which the action shall be performed on
    } - 

    ad_proc -public webix_notification_action_body {} {
        @param action_type_id category "Webix Notification Action Type" Type of action we can perform
        @param action_object_id integer Object which the action shall be performed on
        @param action_text string Text we want to pass through to the action (e.g. the default text for an email)
        @param notification_id object webix_notification::write Notification for which we want to add/edit the action
    } - 

}

ad_proc -public cog_rest::get::webix_notification {
    -rest_user_id:required
    { -notification_id ""}
    { -notification_ids ""}
    { -recipient_id ""}
    { -context_id ""}
    { -project_id ""}
    { -exclude_read "1"}
    { -notification_type_id ""}
} {
    @param notification_id object webix_notification::read Notification which we want to get 
    @param project_id object im_project::read Project for which we want to get the notifications
    @param recipient_id object person::read User for whom we want to read the notifications
    @param context_id integer Context for which we would like to get a list  of notifications
    @param notification_type_id category "Webix Notification Type" Type of notification we are looking for
    @param exclude_read boolean should we exclude read notifications (yes is default)
    @return notifications json_array webix_notification Array of notifications
} {
    set notifications [list]
    set where_clause_list [list]
    if {$notification_id ne ""} {
        lappend where_clause_list "notification_id = :notification_id"
    } 
    
    if {$notification_ids ne ""} {
        lappend where_clause_list "notification_id in ([template::util::tcl_to_sql_list $notification_ids])"
    } 

    if {$project_id ne ""} {
        lappend where_clause_list "project_id = :project_id"
    }

    if {$context_id ne ""} {
        lappend where_clause_list "n.context_id = :context_id"
    }

    if {$recipient_id ne ""} {
        lappend where_clause_list "recipient_id = :recipient_id"
    }

    if {[llength $where_clause_list]<1} {    
        lappend where_clause_list "recipient_id = :rest_user_id"
    }

    if {$exclude_read} {
        lappend where_clause_list "viewed_date is null"
    }

    if {$notification_type_id ne ""} {
        lappend where_clause_list "notification_type_id = :notification_type_id"
    }

    set notification_ids [db_list notifications "
        select notification_id
        from webix_notifications n
        where [join $where_clause_list " and "]
    "]

    foreach notification_id $notification_ids {        
        set actions_arr($notification_id) [cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::webix_notification_action -notification_id $notification_id -rest_user_id $rest_user_id]]
    }

    db_foreach notifications "
        select notification_id, recipient_id, n.context_id, notification_status_id, notification_type_id, message, viewed_date, project_id
        from webix_notifications n
        where [join $where_clause_list " and "]
    " { 
        if {[info exists actions_arr($notification_id)]} {
            set actions $actions_arr($notification_id)
        } else {
            set actions ""
        }
        
        set notification_color [db_string aux_html2 "select aux_html2 from im_categories where category_id = :notification_status_id" -default ""]
        set context [cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $context_id]
        if {$project_id ne ""} {
            set project [cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $project_id]
            set project_nr [db_string project_nr "select project_nr from im_projects where project_id = :project_id" -default ""]
        } else {
            set project ""
            set project_nr ""
        }
		regsub -all {\n} $context {} context
        lappend notifications [cog_rest::json_object]
    }
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::webix_notification {
    -rest_user_id:required
    { -notification_type_id "" }
    { -notification_status_id ""}
    { -message "" }
    -context_id:required
    -recipient_id:required
    { -project_id "" }
} {
    @param webix_notification_body request_body Notification Information

    @return notification json_object webix_notification Object with the created notification
} {
    set notification ""
    set notification_id [webix::notification::create \
        -notification_status_id $notification_status_id \
        -notification_type_id $notification_type_id \
        -context_id $context_id \
        -recipient_id $recipient_id \
        -message $message \
        -creation_user_id $rest_user_id \
        -project_id $project_id
    ]
    
    # cog_log Notice "Notificiation created $notification_id [cog_rest::get::webix_notification -notification_id $notification_id -rest_user_id $rest_user_id]"
    set notification [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::webix_notification -notification_id $notification_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::put::webix_notification {
    -rest_user_id:required
    -notification_id:required
    { -notification_type_id "" }
    { -notification_status_id ""}
    { -message "" }
    { -project_id "" }
    -context_id:required
    -recipient_id:required
} {
    @param webix_notification_body request_body Notification Information
    @param notification_id object webix_notification::write Notification we want to update
    @return notification json_object webix_notification Object with the created notification
} {
    set notification ""
    set notification_id [webix::notification::update \
        -notification_id $notification_id \
        -notification_status_id $notification_status_id \
        -notification_type_id $notification_type_id \
        -context_id $context_id \
        -recipient_id $recipient_id \
        -message $message \
        -creation_user_id $rest_user_id \
        -project_id $project_id
    ]
    
    cog_log Notice "Finished update $notification_id"
    set notification [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::webix_notification -notification_id $notification_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::webix_notification {
    -rest_user_id:required
    -notification_id:required
} {
    Deletes a notification
    @param notification_id object webix_notification::write
    @return errors json_array error Array of errors found

} {
    set errors [list]

    if {[webix::notification::delete -notification_id $notification_id] eq "-1"} {
        set err_msg "Could not delete notifiction"
        set parameter "[im_name_from_id $notification_id]"
        set object_id $notification_id
        lappend errors [cog_rest::json_object]
    }
    return [cog_rest::json_response]

}

ad_proc -public cog_rest::post::webix_notification_read {
    -rest_user_id:required
    -notification_id:required
} {
    Marks a notification as read

    @param notification_id object webix_notification::write Notification we want to mark as read

    @return notification json_object webix_notification Object of notification

} {
    webix::notification::record_view -notification_id $notification_id
    set notification [cog_rest::helper::json_array_to_object -json_array [cog_rest::get::webix_notification -notification_id $notification_id -exclude_read 0 -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::get::webix_notification_action {
    -notification_id:required
    -rest_user_id:required
} {
    @param notification_id object webix_notification::read Notification which we want to get 
    @return actions json_array webix_notification_action Array of actions we can perform on that notification
} {
    set actions [list]
    db_foreach action "select action_type_id, action_object_id, action_text, action_help, recipient_id, aux_html2 as action_icon
        from webix_notification_actions na, webix_notifications n, im_categories c
        where n.notification_id = na.notification_id
        and c.category_id = na.action_type_id
        and n.notification_id = :notification_id
        order by c.sort_order asc" {
        
        set locale [lang::user::locale -user_id $recipient_id]
        if {$action_object_id ne ""} {
            set action_object [cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $action_object_id]
        } else {
            set action_object ""
        }

        lappend actions [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::webix_notification_schedule {
    -rest_user_id:required
    -schedule_proc:required
} {
    Runs a schedule proc for notifications

    @param schedule_proc string Name of the schedule procedure to call in the webix namespace

    @return notifications json_array webix_notification Array of notifications
} {
    set notifications [list]
    set valid_procs [list create_overdue_assignments create_overdue_projects create_unanswered_requested_assignments create_contact_again_projects]
    if {[lsearch $valid_procs $schedule_proc]<0} {
        cog_rest::error -http_status 400 -message "[_ webix-portal.err_invalid_sched_proc]"
    }

    if {[catch "webix::notification::$schedule_proc" notification_ids]} {
        cog_rest::error -http_status 400 -message "[_ webix-portal.err_sched_proc_notif]"
    } else {
        set notifications [cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::webix_notification -notification_ids $notification_ids -rest_user_id $rest_user_id]]
    }

    return [cog_rest::json_response]
}

namespace eval webix::notification {
    ad_proc -public create {
        {-notification_status_id "4240"}
        {-notification_type_id "4280"}
        -context_id:required
        -recipient_id:required
        {-message ""}
        {-project_id ""}
        -creation_user_id:required
    } {
        Creates a new notification to be displayed with webix

        @param notification_status_id Status of the notification / severity - defaults to information 4240
        @param notification_type_id Type of the notification - defaults to 4280 (default)
        @param context_id Object for which we create the notification
        @param project_id Project which this notification happens in
        @param recipient_id Recipient who is receiving the notification
        @param message Message we want to send in the notification
        @param creation_user_id Who is creating this notification

        @return notification_id Id of the created notification
    } {
        if {$message eq "" && $notification_type_id eq ""} {
            return
        }
        
        if {$message eq ""} {
            set locale [lang::user::locale -user_id $recipient_id]
            set message [im_category_string1 -category_id $notification_type_id -locale $locale]
        }
        # Check if we already have an open notification for this object and recipient
        set notification_id [db_string notif_exists "select notification_id from webix_notifications
            where context_id = :context_id and recipient_id = :recipient_id and notification_type_id = :notification_type_id order by notification_id desc limit 1" -default ""]

        if {$notification_id ne ""} {
            # Check if we just need to update the viewed_date and potentially the status
            webix::notification::update -notification_id $notification_id \
                -notification_status_id $notification_status_id \
                -notification_type_id $notification_type_id \
                -context_id $context_id \
                -recipient_id $recipient_id \
                -message $message \
                -project_id $project_id \
                -creation_user_id $creation_user_id 
        } else {

            # Create the notification
            set notification_id [db_string notification "select webix_notification__new(
                :creation_user_id,
                null,
                :notification_type_id,
                :notification_status_id,
                :context_id,
                :recipient_id,
                :message,
                :project_id) from dual"
            ]
        }

        # Always provide the mark as read action
        webix::notification::action_create -notification_id $notification_id -action_type_id 14300

        # Permissions on the notification
        permission::grant -party_id $recipient_id -object_id $notification_id -privilege "write"

        return $notification_id
    }

    ad_proc -public update {
        -notification_id:required
        { -notification_status_id "" }
        { -notification_type_id "" }
        { -context_id "" }
        { -recipient_id "" }
        { -message ""}
        { -creation_user_id "" }
        { -project_id "" }
    } {
        Updates a notification with new data

        @param notification_id Notification we want to update
        @param notification_status_id Status of the notification / severity - defaults to information 4240
        @param notification_type_id Type of the notification - defaults to 4280 (default)
        @param context_id Object for which we create the notification
        @param project_id Project in which this notification was caused
        @param recipient_id Recipient who is receiving the notification
        @param message Message we want to send in the notification
        @param creation_user_id Who is creating this notification

        @return notification_id Id of the created notification
    } {

        # Check if we already have an open notification for this object and recipient
        set notification_id [db_string notif_exists "select notification_id from webix_notifications
            where notification_id = :notification_id" -default ""]

        if {$notification_id eq ""} {
            return ""
        } else {

            # Update the notification

            set update_sql_list [list "viewed_date = null"]
            foreach key [list notification_status_id notification_type_id context_id recipient_id message project_id] {
                set value [set $key]
                if {$value ne ""} {
                    lappend update_sql_list "$key = :$key"
                }
            }
            if {[llength update_sql_list]>0} {
                db_dml update_notification "update webix_notifications set [join $update_sql_list ", "] where notification_id = :notification_id"
            }

            if {$creation_user_id eq ""} {
                set creation_user_id [auth::get_user_id]
            }
            acs_object::update -object_id $notification_id -user_id $creation_user_id
        }

        # Permissions on the notification
        permission::grant -party_id $recipient_id -object_id $notification_id -privilege "write"

        return $notification_id
    }

    ad_proc -public delete {
        -notification_id:required
    } {
        Deletes a notification alongside it's actions

        @param notification_id Notification we want to delete
    } {
        return [db_string delete "select webix_notification__delete(:notification_id) from dual" -default "-1"]
    }

    ad_proc -public action_create {
        -notification_id:required
        -action_type_id:required
        {-action_object_id ""}
        {-action_text ""}
        {-action_help ""}
    } {
        Adds an action to a notification for the user to execute

        @param notification_id Notification which will have this action attached
        @param action_type_id What kind of action do we want to add.
        @param action_object_id On what object do we want to run the action
        @param action_text Text to pass through to the action (e.g. email text to send)
        @param action_help Help Text to display on mouseover of the action icon
    } {
        set action_exists_p [db_string action_exists "select 1 from webix_notification_actions where notification_id = :notification_id
            and action_type_id = :action_type_id" -default 0]
        
        if {$action_help eq ""} {
            set action_help [im_category_string1 -category_id $action_type_id]
        }
        if {!$action_exists_p} {
            db_dml insert_action "insert into webix_notification_actions 
                (notification_id,action_type_id,action_object_id,action_text, action_help)            
                values
                (:notification_id,:action_type_id,:action_object_id,:action_text, :action_help)"
        } else {
            webix::notification::action_update -notification_id $notification_id -action_type_id $action_type_id -action_object_id $action_object_id -action_text $action_text -action_help $action_help
        }
        return 1
    }


    ad_proc -public action_object {
        -rest_user_id:required
        -action_type_id:required
        {-action_object_id ""}
        {-action_text ""}
        {-action_help ""}
        {-action_icon ""}
        
    } {
        Returns a json_object for the action

        @param rest_user_id User who will see the action
        @param action_type_id What kind of action do we want to add.
        @param action_object_id On what object do we want to run the action
        @param action_text Text to pass through to the action (e.g. email text to send)
        @param action_help Help Text to display on mouseover of the action icon
    } {
        if {$action_help eq ""} {
            set locale [util_memoize [list lang::user::locale -user_id $rest_user_id] 3600]
            set action_help [util_memoize [list im_category_string1 -category_id $action_type_id -locale $locale] 3600]
        }

        if {$action_object_id ne ""} {
            set action_object [cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $action_object_id]
        } else {
            set action_object ""
        }
        if {$action_icon eq ""} {
            set action_icon [util_memoize [list db_string action_icon "select aux_html2 as action_icon from im_categories where category_id = $action_type_id" -default ""] 3600]
        }
        return [cog_rest::json_object -object_class "webix_notification_action"]
    }

    ad_proc -public action_update {
        -notification_id:required
        -action_type_id:required
        {-action_object_id ""}
        {-action_text ""}
        {-action_help ""}

    } {
        Updates an action to a notification for the user to execute

        @param notification_id Notification which will have this action attached
        @param action_type_id What kind of action do we want to add.
        @param action_object_id On what object do we want to run the action
        @param action_text Text to pass through to the action (e.g. email text to send)
        @param action_help Help Text to display on mouseover of the action icon

    } {
        set action_exists_p [db_string action_exists "select 1 from webix_notification_actions where notification_id = :notification_id
            and action_type_id = :action_type_id" -default 0]
        
        if {$action_exists_p} {
            db_dml update_action "update webix_notification_actions 
                set action_object_id = :action_object_id,
                action_text = :action_text,
                action_help = :action_help
                where notification_id = :notification_id
                and action_type_id = :action_type_id"                
        } else {
            webix::notification::action_create -notification_id $notification_id -action_type_id $action_type_id -action_object_id $action_object_id -action_text $action_text -action_help $action_help
        }
        return 1
    }

    ad_proc -public action_delete {
        -notification_id:required
        { -action_type_id ""}
    } {
        Deletes an action from a notification

        @param notification_id notification we want to remove the action from
        @param action_type_id Type of action we want to remove - if empty remove all actions from the notification
    } {
        set where_clause_list [list "notification_id = :notification_id"]
        if {$action_type_id ne ""} {
            lappend where_clause_list "action_type_id = :action_type_id"
        }

        db_string delete "delete from webix_notification_actions where [join $where_clause_list " and "]"
    }

    ad_proc -public record_view {
        { -context_id ""}
        { -viewer_id ""}
        { -notification_id ""}
    } {
        Records the viewing of a notification
        @param context_id Context Object which has the notification
        @param viewer_id For whom do we want to record the viewing of the notification

        @return notification_id in case we found and updated it.
    } {
        if {$notification_id eq ""} {
            set notification_id [db_string notification_id "select notification_id from webix_notifications  where context_id = :context_id and recipient_id = :viewer_id"]
        }

        if {$notification_id ne ""} {
            db_dml viewed "update webix_notifications set viewed_date = now() where notification_id = :notification_id"
        }
        
        return $notification_id
    }

    ad_proc -public create_overdue_projects {} {
        Loop through all projects which are open and where the due date is in the past or upcoming
    } {
        set notifications_ids [list]

        # ---------------------------------------------------------------
        # Due soon Projects
        # ---------------------------------------------------------------
        set open_status_ids [im_sub_categories [im_project_status_open]]
        db_foreach projects_due_soon "select project_id, project_lead_id from im_projects
            where end_date > now()
            and end_date <= now() + interval '1 hour'
            and project_status_id in ([template::util::tcl_to_sql_list $open_status_ids])" {
            
            lappend notification_ids [webix::notification::create \
                -context_id $project_id \
                -project_id $project_id \
                -notification_type_id [webix_notification_type_project_overdue] \
                -notification_status_id [webix_notification_status_warning] \
                -recipient_id $project_lead_id \
                -creation_user_id $assignee_id]

        }
        
        # ---------------------------------------------------------------
        # Overdue Projects
        # ---------------------------------------------------------------

        db_foreach projects_overdue "select project_id, project_lead_id from im_projects
            where end_date < now()
            and project_status_id in ([template::util::tcl_to_sql_list $open_status_ids])" {

            lappend notification_ids [webix::notification::create \
                -context_id $project_id \
                -project_id $project_id \
                -notification_type_id [webix_notification_type_project_overdue] \
                -notification_status_id [webix_notification_status_important] \
                -recipient_id $project_lead_id \
                -creation_user_id $project_lead_id]
        
        }

        return $notification_ids
    }

    ad_proc -public create_contact_again_projects {} {
        Loop through all projects which are potential and where the contact again dates are in the past
    } {
        # ---------------------------------------------------------------
        # Due soon Projects
        # ---------------------------------------------------------------
        set not_in_project_status_id [im_sub_categories [im_project_status_closed]]
        set notifications_ids [list]
        db_foreach projects_due_soon "select project_id, project_nr, im_name_from_id(company_id) as company , 
            im_name_from_id(company_contact_id) as company_contact, company_contact_id, project_lead_id from im_projects
            where date(contact_again_date) = date(now())
            and project_status_id not in ([template::util::tcl_to_sql_list $not_in_project_status_id])" {

            set notification_type_id [webix_notification_type_project_contact_again]
            set notification_id [db_string notif_exists "select notification_id from webix_notifications
                where context_id = :project_id and recipient_id = :project_lead_id and notification_type_id = :notification_type_id order by notification_id desc limit 1" -default ""]
            if {$notification_id eq ""} {
                set notification_id [webix::notification::create \
                    -context_id $project_id \
                    -project_id $project_id \
                    -notification_type_id $notification_type_id \
                    -notification_status_id [webix_notification_status_warning] \
                    -recipient_id $project_lead_id \
                    -creation_user_id $project_lead_id \
                    -message "[_ webix-portal.notif_contact_again]"]

                webix::notification::action_create \
                    -notification_id $notification_id \
                    -action_type_id [webix_notification_action_type_send_email] \
                    -action_object_id $company_contact_id
            }
            lappend notification_ids $notification_id
        }
        return $notifications_ids
    }

    ad_proc -public create_overdue_assignments {
        -scan_due_soon:boolean
    } {
        Loop through all assignments which are open and where the due date is in the past or upcoming
    } {
        # ---------------------------------------------------------------
        # Due soon Assignments
        # ---------------------------------------------------------------
        set open_status_ids [list 4222 4224 4229]
        set notification_type_id [webix_notification_type_assignment_overdue]
        set notifications_ids [list]

        if {$scan_due_soon_p} {
            db_foreach assignments_due_soon "select p.project_id,p.project_nr,assignment_name, assignment_id, assignee_id, project_lead_id
                from im_freelance_assignments fa, im_freelance_packages fp, im_projects p
                where fa.freelance_package_id = fp.freelance_package_id
                and fa.end_date > now()
                and fa.end_date <= now() + interval '1 hour'
                and p.project_id = fp.project_id
                and assignment_status_id in ([template::util::tcl_to_sql_list $open_status_ids])" {

                set assignee_name [im_name_from_id $assignee_id]

                # Check if we already have an open notification for this object and recipient
                set notification_id [db_string notif_exists "select notification_id from webix_notifications
                    where context_id = :assignment_id and recipient_id = :project_lead_id and notification_type_id = :notification_type_id order by notification_id desc limit 1" -default ""]
                if {$notification_id eq ""} {

                    set notification_id [webix::notification::create \
                        -context_id $assignment_id \
                        -project_id $project_id \
                        -notification_type_id $notification_type_id \
                        -notification_status_id [webix_notification_status_default] \
                        -recipient_id $project_lead_id \
                        -creation_user_id $assignee_id \
                        -message "[_ webix-portal.assignement_due_soon_user]"] 
                    
                    webix::notification::action_create \
                        -notification_id $notification_id \
                        -action_type_id [webix_notification_action_type_send_email] \
                        -action_object_id $assignee_id
                }
                lappend notification_ids $notification_id
                
                set notification_id [db_string notif_exists "select notification_id from webix_notifications
                    where context_id = :assignment_id and recipient_id = :assignee_id and notification_type_id = :notification_type_id order by notification_id desc limit 1" -default ""]
                if {$notification_id eq ""} {
                    
                    set notification_id [webix::notification::create \
                        -context_id $assignment_id \
                        -project_id $project_id \
                        -notification_type_id $notification_type_id \
                        -notification_status_id [webix_notification_status_warning] \
                        -recipient_id $assignee_id \
                        -creation_user_id $project_lead_id \
                        -message "[_ webix-portal.assignement_due_soon_project]"]

                    webix::notification::action_create \
                        -notification_id $notification_id \
                        -action_type_id [webix_notification_action_type_send_email] \
                        -action_object_id $project_lead_id
                }
                lappend notification_ids $notification_id
            }
        }

        # ---------------------------------------------------------------fdb
        # Overdue Assignments
        # ---------------------------------------------------------------

        db_foreach assignments_overdue "select p.project_id,p.project_nr, assignment_name, assignment_id, assignee_id, project_lead_id
            from im_freelance_assignments fa, im_freelance_packages fp, im_projects p
            where fa.freelance_package_id = fp.freelance_package_id
            and p.project_id = fp.project_id
            and fa.end_date < now()
            and assignment_status_id in ([template::util::tcl_to_sql_list $open_status_ids])" {

            set assignee_name [im_name_from_id $assignee_id]

            set notification_id [db_string notif_exists "select notification_id from webix_notifications
                where context_id = :assignment_id and recipient_id = :project_lead_id and notification_type_id = :notification_type_id and notification_status_id = [webix_notification_status_warning]
                order by notification_id desc limit 1
                 " -default ""]
            if {$notification_id eq ""} {
                set notification_id [webix::notification::create \
                    -context_id $assignment_id \
                    -project_id $project_id \
                    -notification_type_id [webix_notification_type_assignment_overdue] \
                    -notification_status_id [webix_notification_status_warning] \
                    -recipient_id $project_lead_id \
                    -creation_user_id $assignee_id \
                    -message "[_ webix-portal.assignment_overdue_user_1]"] 


                webix::notification::action_create \
                    -notification_id $notification_id \
                    -action_type_id [webix_notification_action_type_send_email] \
                    -action_object_id $assignee_id
            }
            lappend notification_ids $notification_id

            set notification_id [db_string notif_exists "select notification_id from webix_notifications
                where context_id = :assignment_id and recipient_id = :assignee_id and notification_type_id = :notification_type_id and notification_status_id = [webix_notification_status_important]
                order by notification_id desc limit 1" -default ""]
            if {$notification_id eq ""} {
                set notification_id [webix::notification::create \
                    -context_id $assignment_id \
                    -project_id $project_id \
                    -notification_type_id [webix_notification_type_assignment_overdue] \
                    -notification_status_id [webix_notification_status_important] \
                    -recipient_id $assignee_id \
                    -creation_user_id $project_lead_id \
                    -message "[_ webix-portal.assignment_overdue_project_1]"]
                
                webix::notification::action_create \
                    -notification_id $notification_id \
                    -action_type_id [webix_notification_action_type_send_email] \
                    -action_object_id $project_lead_id
            }
            lappend notification_ids $notification_id
        }
        return $notification_ids
    }

    ad_proc -public create_unanswered_requested_assignments {} {
        Loop though requested assignments and remind PMs if freelancers haven't reacted
    } {
        set notifications_ids [list]

        db_foreach requested_assignment "select fa.assignment_id, fp.freelance_package_id, fp.freelance_package_name, p.project_nr, p.project_lead_id, p.project_id, fa.assignee_id
            from im_freelance_assignments fa, acs_objects o, im_freelance_packages fp, im_projects p
            where assignment_status_id = [translation_assignment_status_requested]
            and last_modified < now() - interval '2 hours'
            and o.object_id = fa.assignment_id
            and fa.freelance_package_id = fp.freelance_package_id
            and p.project_id = fp.project_id
        " {
            set notification_type_id [webix_notification_type_assignment_request_out]

            set notification_id [db_string notif_exists "select notification_id from webix_notifications
                where context_id = :assignment_id and recipient_id = :project_lead_id and notification_type_id = :notification_type_id and notification_status_id = [webix_notification_status_warning]
                order by notification_id desc limit 1" -default ""]
            set assignee_name [im_name_from_id $assignee_id]
            if {$notification_id eq ""} {
                set notification_id [webix::notification::create \
                    -context_id $assignment_id \
                    -notification_type_id [webix_notification_type_assignment_request_out] \
                    -notification_status_id [webix_notification_status_warning] \
                    -recipient_id $project_lead_id \
                    -message "[_ webix-portal.assignment_not_reacted]" \
                    -creation_user_id $project_lead_id]

                webix::notification::action_create \
                    -notification_id $notification_id \
                    -action_type_id [webix_notification_action_type_send_email] \
                    -action_object_id $assignee_id

                webix::notification::action_create \
					-notification_id $notification_id \
					-action_type_id [webix_notification_action_type_open_assignment_overview] \
					-action_object_id $assignment_id 
            }
            lappend notification_ids $notification_id
        }
        return $notification_ids
    }
}
