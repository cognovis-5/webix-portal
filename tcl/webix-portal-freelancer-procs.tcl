ad_library {
    Procedures for working with freelancers
    @author malte.sussdorff@cognovis.de
}
namespace eval cog_rest::json_object {
    ad_proc freelancer_language {} {
        @return language category "Intranet Translation Language" Target language for which this freelancer can work on
        @return type string Type of language (source or target)
    } -
}


ad_proc -public cog_rest::get::freelancer_project_languages {
    -freelancer_ids:required
    -project_id:required
    -rest_user_id:required
} {
    @param freelancer_ids object_array person::read IDs of freelancers we want to check the project language for
    @param project_id object im_project::read ID of the project for which we look up the freelancer languages

    @return project_languages json_array freelancer_language Array of languages a freelancer can work on in this project
} {
    set project_languages [list]
    set target_language_ids [webix::freelancer::project_target_languages -freelancer_ids $freelancer_ids -project_id $project_id] 
    foreach language_id $target_language_ids {
        set type "target"
        lappend project_languages [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}

#---------------------------------------------------------------
# Object Main Translator
#---------------------------------------------------------------

ad_proc -public cog_rest::json_object::main_translator {
} {
    @return mapping_id number id of mapping
    @return freelancer object person information of main translator
    @return customer named_id company for which user is main translator
    @return source_language category "Intranet Translation Languages" source language (skill)
    @return target_language category "Intranet Translation Languages" target language (skill)
} -

ad_proc -public cog_rest::json_object::main_translator_body {
} {
    @param freelancer_id object person User (freelancer) who will become main translator for give company
    @param customer_id object im_company::read Company for which user will become main translator
    @param source_language_id category "Intranet Translation Language" Source Language for which use will become main translator
    @param target_language_id category "Intranet Translation Language" Target Language for which use will become main translator
} -


#---------------------------------------------------------------
# Object Skills
#---------------------------------------------------------------

ad_proc -public cog_rest::json_object::object_skill {
} {
    Specific skills for objects - see im_object_freelance_skill_map

    @return object named_id Object we attach the skill to
    @return skill named_id Skill which is required
    @return skill_type category "Intranet Skill Type" Skill type of the required skill
    @return claimed_experience category "Intranet Experience Level" Level of experience claimed
    @return confirmed_experience category "Intranet Experience Level" Level of experience confirmed
    @return required_experience category "Intranet Experience Level" Level of experience required
    @return skill_weight number How important is this skill for this object in relation to others
    @return skill_required_p boolean Is the skill required for this object
} -

ad_proc -public cog_rest::json_object::freelancer_skill {
} {
    Specific skills for objects - see im_object_freelance_skill_map

    @return user object person Freelancer posessing the skill
    @return skill category Skill which is required
    @return skill_type category "Intranet Skill Type" Skill type of the required skill
    @return claimed_experience category "Intranet Experience Level" Level of experience claimed
    @return confirmed_experience category "Intranet Experience Level" Level of experience confirmed
    @return confirmation_user object person Person who confirmed the experience
} -


ad_proc -public cog_rest::json_object::freelancer_skill_body {
} {
    Specific skills for objects - see im_object_freelance_skill_map

    @param user_id object person Freelancer posessing the skill
    @param skill_id integer Skill which is required
    @param skill_type_id category "Intranet Skill Type" Skill type of the required skill
    @param claimed_experience_id category "Intranet Experience Level" Level of experience claimed
    @param confirmed_experience_id category "Intranet Experience Level" Level of experience confirmed
    @param confirmation_user_id object person Person who confirmed the experience
} -

ad_proc -public cog_rest::get::object_skill {
    -object_id:required
    { -skill_type_id "" }
    -rest_user_id:required
} {
    Return a list of required skills for an object

    @param object_id integer Object ID of the object (typically project, RFQ, other) for which we want to see required skills
    @param skill_type_id category "Intranet Skill Type" Skill type we want to filter for

    @return skills json_array object_skill Array of object Skills
} {
    set skills [list]
    set where_clauses [list]
    lappend where_clauses "object_id = :object_id"
    if {$skill_type_id ne ""} {
        lappend where_clauses "skill_type_id = :skill_type_id"
    }

    db_foreach skill "
        select skill_id, skill_type_id, object_id, skill_weight, skill_required_p,
            required_experience_id, claimed_experience_id, confirmed_experience_id
        from im_object_freelance_skill_map where [join $where_clauses " and "]
    " { 
        set skill [cog_rest::json_object -object_class "skill"]
        lappend skills [cog_rest::json_object]
    }

    return [cog_rest::json_response]

}

ad_proc -public cog_rest::post::object_skill {
    -object_id:required
    -skill_ids:required
    -rest_user_id:required
} {
    Return a list of required skills for an object

    @param object_id object *::read ID of the object (typically project, RFQ, other) for which we want to see required skills
    @param skill_ids category_array "*" Skills of the skill type we want to filter for

    @return skills json_array object_skill Array of object_skill
} {

    set skill_type_ids [list]
    foreach skill_id $skill_ids {
        set skill_type_id [webix::freelancer::get_skill_type_id -skill_id $skill_id]
        if {$skill_type_id ne ""} {
            lappend skill_type_ids $skill_type_id
            im_freelance_add_required_skills -object_id $object_id -skill_type_id $skill_type_id -skill_ids $skill_id
        }
    }

    set skills [ cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::object_skill -object_id $object_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::object_skill {
    -object_id:required
    { -skill_id "" }
    { -skill_type_id ""}
    -rest_user_id:required
} {
    Deletes a skill from an object

    @param object_id object *::read ID of the object (typically project, RFQ, other) from which we want to delete the skill
    @param skill_id category "*" Skills we want to delete
    @param skill_type_id category "Intranet Skill Type" Skill type we want delete from the object

    @return skills json_array object_skill Array of skills after deletion. Only return those of the skill_type associated with the skill_ids
} {

    set where_clause_list [cog_rest::parse::where_clauses]
    
    db_dml delete "
		delete from im_object_freelance_skill_map
		where [join $where_clause_list " and "]
		"

    set skills [ cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::object_skill -object_id $object_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}


#---------------------------------------------------------------
# Main translators
#---------------------------------------------------------------


ad_proc -public cog_rest::get::main_translators {
    -freelancer_id
    -customer_id
    -rest_user_id:required
} {
    Return a list of main translators for given user_id or company_id

    @param freelancer_id object person::read Freelancer for whom we want to get list of companies where he works as main translator
    @param customer_id im_company::read Company for which we want to display main translators

    @return main_translators json_array main_translator
} {

    set main_translators [list]

    set where_clause_list [cog_rest::parse::where_clauses]
   
    set sql "select mapping_id, customer_id, im_name_from_id(customer_id) as customer_name, freelancer_id,
		im_name_from_id(freelancer_id) as freelancer_name, source_language_id, im_name_from_id(source_language_id) as source_language, target_language_id, im_name_from_id(target_language_id) as target_language
		from im_trans_main_freelancer
		where [join $where_clause_list " and "]
		order by customer_name, freelancer_name, source_language, target_language"

    db_foreach user $sql {
        lappend main_translators [cog_rest::json_object]
    }

    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::main_translator {
    -freelancer_id:required
    -customer_id:required
    -source_language_id:required
    -target_language_id:required
    -rest_user_id
} {
    Add a main translator to a company

    @param main_translator_body request_body data we need to create a new mapping (user_id, company_id, source_language_id, target_language_id)

    @return main_translators json_array main_translator    
} {
    set main_translators [list]
    # Probably needs check if such mapping already exists
    db_dml insert "insert into im_trans_main_freelancer (customer_id, freelancer_id, source_language_id, target_language_id) values (:customer_id, :freelancer_id, :source_language_id, :target_language_id)"

    # Always returning empty list for now
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::delete::main_translator {
    -mapping_id:required
    -rest_user_id
} {
    Delete a skill from a user
    @param mapping_id id of mapping which we want to delete

    @return main_translators json_array main_translator
} {

    set main_translators [list]
    db_dml main_translator_delete "delete from im_trans_main_freelancer where mapping_id = :mapping_id"

    # Always returning empty list for now
    return [cog_rest::json_response]
}


#---------------------------------------------------------------
# Freelancer Skills
#---------------------------------------------------------------

ad_proc -public cog_rest::get::freelancer_skill {
    -user_id:required
    -skill_type_id
    -rest_user_id:required
} {
    Return a list of skills for the freelancer or project.

    @param user_id object person::read Freelancer we would like to get the skills for
    @param skill_type_id category "Intranet Skill Type" Skill type we want to filter for

    @return freelance_skills json_array freelancer_skill
} {
    set freelance_skills [list]
    set where_clause_list [cog_rest::parse::where_clauses]
    lappend where_clause_list "c.category_id = fs.skill_type_id"
    db_foreach skill "select skill_id, im_name_from_id(skill_id) as skill_name, skill_type_id, claimed_experience_id, confirmed_experience_id, confirmation_user_id
        from im_freelance_skills fs, im_categories c
        where [join $where_clause_list " and "]
        order by c.sort_order, skill_name" {
            lappend freelance_skills [cog_rest::json_object]
        }

    return [cog_rest::json_response]
}


ad_proc -public cog_rest::put::freelancer_skill {
    -user_id:required
    -skill_type_id:required
    -skill_id:required
    -claimed_experience_id
    -confirmed_experience_id
    -confirmation_user_id
    -rest_user_id
} {
    Add skills to an object (typically a project or a freelancer)

    @param freelancer_skill_body request_body Skills we would like to add for the freelancer

    @return freelance_skills json_array freelancer_skill    
} {

    set set_clause_list [ cog_rest::parse::set_clauses -ignore_params_list [list user_id skill_type_id skill_id]]

    db_dml update "update im_freelance_skills set [join $set_clause_list ", "] where 
        user_id = :user_id and skill_type_id = :skill_type_id and skill_id = :skill_id"

    set freelance_skills [ cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::freelancer_skill -user_id $user_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}

ad_proc -public cog_rest::post::freelancer_skill {
    -user_id:required
    -skill_type_id:required
    -skill_id:required
    { -claimed_experience_id "" }
    { -confirmed_experience_id "" }
    { -confirmation_user_id "" }
    -rest_user_id
} {
    Add skills to an object (typically a project or a freelancer)

    @param freelancer_skill_body request_body Skills we would like to add for the freelancer

    @return freelance_skills json_array freelancer_skill    
} {
    set skill_exists_p [db_string skill_exists "select 1 from im_freelance_skills where skill_id = :skill_id and user_id = :user_id" -default 0]
    if {$skill_exists_p} {
        return [cog_rest::put::freelancer_skill -user_id $user_id -skill_type_id $skill_type_id -skill_id $skill_id \
            -claimed_experience_id $claimed_experience_id -confirmed_experience_id $confirmed_experience_id -confirmation_user_id $confirmation_user_id \
            -rest_user_id $rest_user_id
        ]
    } else {
        db_dml add_skill "insert into im_freelance_skills (user_id, skill_type_id, skill_id, claimed_experience_id, confirmed_experience_id, confirmation_user_id)
            values (:user_id, :skill_type_id, :skill_id, :claimed_experience_id, :confirmed_experience_id, :confirmation_user_id)"
        set freelance_skills [ cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::freelancer_skill -user_id $user_id -rest_user_id $rest_user_id]]
        return [cog_rest::json_response]
    }
}

ad_proc -public cog_rest::delete::freelancer_skill {
    -user_id:required
    -skill_id:required
    -rest_user_id
} {
    Delete a skill from a user
    @param user_id object person::write User who's skillset we want to remove
    @param skill_id integer Category for the skill we want to remove

    @return freelance_skills json_array freelancer_skill List of remaining skills
} {
    db_dml delete "delete from im_freelance_skills where skill_id = :skill_id and user_id = :user_id"
    set freelance_skills [ cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::freelancer_skill -user_id $user_id -rest_user_id $rest_user_id]]
    return [cog_rest::json_response]
}


namespace eval webix::freelancer {
    ad_proc -public project_target_languages {
        -freelancer_ids:required
        -project_id:required
    } {
        @param freelancer_ids IDs of freelancers we want to check the project language for
        @param project_id ID of the project for which we look up the freelancer languages

        @return  List of language_ids the freelancer can translate into in this project
    } {
        set project_lang_ids [list]
        
        # Get the list of possible target languages in the project
        set language_sql "select  language_id as target_language_id, parent_id
            from    im_target_languages left outer join im_category_hierarchy ch on (child_id = language_id)
            where   project_id = :project_id"
        
        db_foreach language $language_sql {
            if {[exists_and_not_null parent_id]} {
                set project_lang_ids [concat $project_lang_ids [im_sub_categories $parent_id]]
            } else {
                set project_lang_ids [concat $project_lang_ids [im_sub_categories $target_language_id]]
            }
	    }
	
	    set skill_type_id 2002

        set freelancer_lang_ids [db_list fl_languages "select distinct skill_id from im_freelance_skills 
            where skill_type_id=:skill_type_id 
            and skill_id in ([template::util::tcl_to_sql_list $project_lang_ids])
            and user_id in ([template::util::tcl_to_sql_list $freelancer_ids])"] 

        set freelancer_lang_ids [lsort -unique $freelancer_lang_ids]

        # Append the parent lang_ids
        if {[llength $freelancer_lang_ids]>0} {
            set parent_ids [db_list freelancer_parent_langs "select distinct parent_id from im_category_hierarchy where child_id in ([template::util::tcl_to_sql_list $freelancer_lang_ids])"]
            set freelancer_lang_ids [concat $parent_ids $freelancer_lang_ids]
        }
        
        return [lsort -unique $freelancer_lang_ids]
    }

    ad_proc -public get_skill_type_id {
        -skill_id:required
    } {
        Return the skill_type_id for a given skill

        @param skill_id Skill we look for the type_id
    } {
        set category_type [db_string type "select category_type from im_categories where category_id = :skill_id" -default ""]
        if {$category_type eq ""} {
            return ""
        }

        set skill_type_id [db_string get_type_id "select category_id from im_categories where category_type = 'Intranet Skill Type' and aux_string1 = :category_type" -default ""]
        return $skill_type_id
    }

}
