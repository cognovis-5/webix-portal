ad_library {
    REST Procedures for Search and it's services / implementations
    @author malte.sussdorff@cognovis.de
}

namespace eval cog_rest::json_object {
    ad_proc -public search_result {} {
        @return object json_object cognovis_object The object of the search result
        @return search_text string Text we display for the search result
        @return actions json_array webix_notification_action Actions for  the notification
        @return sort_order integer order by which to order the search result
    } -
}

ad_proc cog_rest::get::webix_search {
    -rest_user_id:required
    -query:required
    {-object_type ""}
} {
    @param query string String which we are querying for

    @return search_results json_array search_result Results which match the query and for which the current user has access
} {
    set query [string tolower $query]
    regsub -all {["']} $query {} query

    # Remove accents and other special characters from search query 
    # set query [db_exec_plsql normalize "select norm_text(:query)"]

    set search_results [list]
    
    if {$object_type eq ""} {
        set object_types [list "im_project" "im_company" "person" "im_invoice"]
    } else {
        set object_types $object_type
    }
    foreach object_type $object_types {
        set search_results [concat $search_results [webix::search::${object_type} -query $query -rest_user_id $rest_user_id]]
    }

    return [cog_rest::json_response]
}

namespace eval webix::search {
    ad_proc -public im_project {
        -rest_user_id:required
        -query:required
    } {
        @param query string String which we are querying for

        @return list of search results json objects
    } {    
        set search_results [list] 
        set project_perm_sql "
            and p.project_id in (
                select
                    p.project_id
                from
                    im_projects p,
                    acs_rels r
                where
                    r.object_id_one = p.project_id
                    and r.object_id_two = :rest_user_id
        )"

        if {[im_permission $rest_user_id "view_projects_all"]} {
            set project_perm_sql ""
        }
        
        set max_results [parameter::get_from_package_key -package_key "webix-portal" -parameter "MaxSearchResults" -default 30]
        set project_sql "select distinct p.project_id, p.project_nr, p.project_name, c.company_name, p.company_id, ca.sort_order, p.end_date
            from im_projects p, im_companies c, im_categories ca
            where (p.project_nr like '%${query}%' 
            or lower(p.project_name) like '%${query}%'
            or lower(c.company_name) like '%${query}%')
            $project_perm_sql
            and project_type_id not in (100,101)
            and p.company_id = c.company_id
            and ca.category_id = p.project_status_id
            order by sort_order asc, p.project_nr desc
            limit $max_results"
        
        set sort_order 0
        set action_type_id [webix_notification_action_type_open_company]
        set action_icon [db_string action_icon "select aux_html2 as action_icon from im_categories where category_id = :action_type_id"]
        set action_help [im_category_string1 -category_id $action_type_id -locale [lang::user::locale -user_id $rest_user_id]]

        db_foreach project $project_sql {
            set object [util_memoize [list cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $project_id] 120]
            set actions [list]
            lappend actions [util_memoize [list webix::notification::action_object -action_type_id $action_type_id -rest_user_id $rest_user_id -action_object_id $company_id \
                -action_icon $action_icon -action_help $action_help] 120]
            set search_text "${project_nr}: $project_name (${company_name})"
            incr sort_order
            lappend search_results "[cog_rest::json_object -object_class "search_result"]"
        }
        return $search_results
    }

    ad_proc -public im_company {
        -rest_user_id:required
        -query:required
    } {
        @param query string String which we are querying for

        @return array of search results
    } {  
        set search_results [list]
        set company_perm_sql "
                    and c.company_id in (
                            select  c.company_id
                            from    im_companies c,
                                    acs_rels r
                            where   r.object_id_one = c.company_id
                                    and r.object_id_two = :rest_user_id
                            and c.company_status_id not in ([im_company_status_deleted])
                    )"

        if {[im_permission $rest_user_id "view_companies_all"]} {
                set company_perm_sql "
                    and c.company_status_id not in ([im_company_status_deleted])
            "
        }

        set company_sql "select distinct c.company_id, address_country_code, company_name
            from im_companies c, im_offices o
            where lower(company_name) like '%${query}%' 
            and c.main_office_id = o.office_id
            $company_perm_sql
            order by company_name"
        
        set sort_order 0
        db_foreach company $company_sql {
            set object [util_memoize [list cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $company_id] 120]
            set search_text "${company_name} (${address_country_code})"
            incr sort_order
            set actions ""
            lappend search_results "[cog_rest::json_object -object_class "search_result"]"
        }

        return $search_results
    }

    ad_proc -public person {
        -rest_user_id:required
        -query:required
    } {
        @param query string String which we are querying for

        @return array of search results for persons
    } {  
        set search_results [list]

        set query_items [split $query " "]

        if {[llength $query_items] eq 2} {
            set person_sql "select distinct person_id, first_names, last_name, email from cc_users 
                where (
                    lower(first_names) like '%[lindex $query_items 0]%' 
                    and lower(last_name) like '%[lindex $query_items 1]%' 
                )
                order by first_names
            "
        } else {
            set query [lindex $query_items 0]
            set person_sql "select distinct person_id, first_names, last_name, email from cc_users 
                where (
                    lower(first_names) like '%${query}%' 
                    or lower(last_name) like '%${query}%' 
                    or lower(email) like '%${query}%' 
                )
                order by first_names
            "
        }
         
        set sort_order 0
        set action_type_id [webix_notification_action_type_send_email]
        set action_icon [db_string action_icon "select aux_html2 as action_icon from im_categories where category_id = :action_type_id"]
        set action_help [im_category_string1 -category_id $action_type_id -locale [lang::user::locale -user_id $rest_user_id]]

        db_foreach person $person_sql {
            im_user_permissions $rest_user_id $person_id view read write admin
            if {$read} {
                set object [util_memoize [list cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $person_id] 120]
                set search_text "$first_names $last_name (${email})"
                incr sort_order
                set actions [list]
                lappend actions [util_memoize [list webix::notification::action_object -action_type_id $action_type_id \
                    -rest_user_id $rest_user_id -action_object_id $person_id \
                    -action_icon $action_icon -action_help $action_help] 120]
                lappend search_results "[cog_rest::json_object -object_class "search_result"]"
            }
        }

        return $search_results
    }

    ad_proc -public im_invoice {
        -rest_user_id:required
        -query:required
    } {
        @param query string String which we are querying for

        @return array of search results for invoices
    } {  
        set search_results [list]

        set invoice_sql "select distinct invoice_id, invoice_nr, i.company_contact_id,
            c.cost_type_id, c.customer_id, c.provider_id, c.project_id, p.project_type_id, p.project_nr
            from im_invoices i, im_costs c left outer join im_projects p on (p.project_id = c.project_id)
            where (
                lower(invoice_nr) like '%${query}%' 
            )
            and i.invoice_id = c.cost_id
            order by cost_type_id, invoice_nr desc
            limit 30
        "
        
        set sort_order 0

        db_foreach invoice $invoice_sql {
            cog::cost::permissions $rest_user_id $invoice_id view read write admin
            if {$read} {
                set object [util_memoize [list cog_rest::get::cognovis_object -rest_user_id $rest_user_id -object_id $invoice_id] 120]
                if {[im_cost_type_is_invoice_or_quote_p $cost_type_id]} {
                    set company_name [im_name_from_id -cache $customer_id]
                } else {
                    set company_name [im_name_from_id -cache $provider_id]
                }
                set search_text "$invoice_nr ($company_name)"
                incr sort_order
                set actions [list]
                set item_id [db_string item "select max(item_id) from cr_items where parent_id = :invoice_id and storage_type = 'file'" -default ""]
                if {$item_id ne ""} {
                    lappend actions [util_memoize [list webix::notification::action_object -action_type_id [webix_notification_action_type_cr_direct_download] \
                        -rest_user_id $rest_user_id -action_object_id $item_id] 120]
                }
                if {$project_id ne ""} {
                    set action_icon [db_string action_icon "select aux_html2 as action_icon from im_categories where category_id = :project_type_id"]
                    set action_help "Open project $project_nr $project_type_id"

                    lappend actions [util_memoize [list webix::notification::action_object -action_type_id [webix_notification_action_type_open_object] \
                        -rest_user_id $rest_user_id -action_object_id $project_id -action_icon $action_icon -action_help $action_help] 120]
                }
                if {$company_contact_id ne ""} {
                    lappend actions [util_memoize [list webix::notification::action_object -action_type_id [webix_notification_action_type_send_email] \
                        -rest_user_id $rest_user_id -action_object_id $company_contact_id \
                        -action_help "Contact [im_name_from_id -cache $company_contact_id] about the invoice"] 120]
                }                
                lappend search_results "[cog_rest::json_object -object_class "search_result"]"
            }
        }

        return $search_results
    }
}