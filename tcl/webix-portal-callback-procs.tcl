ad_library {

    Callbacks for webix-portal module
    
    @author malte.sussdorff@cognovis.de

}

ad_proc -public -callback webix_trans_task_after_create {
    {-project_id:required}
    {-trans_task_ids:required}
    { -user_id "" }
} {
    After creation of trans tasks in a project

    @param project_id Project in which the trans tasks were created
    @param trans_task_ids Tasks which were created
    @param user_id User which is responsible for the task creation
} - 

ad_proc -public -callback webix_trans_task_after_update {    
    {-trans_task_id:required}
} {
    After update of a trans task
    
    @param trans_task_id Task which was updated
} - 


ad_proc -public -callback webix::trans::project::after_create {
    {-project_id:required}
    { -user_id "" }
} {
    After creation of a translation project

    @param project_id Project which was just created
    @param user_id User which is responsible for the project_creation
} -

ad_proc -public -callback webix::trans::project::task_types {
    {-project_id:required}
    {-project_type_id:required}
    { -user_id "" }
} {
    Callback to allow the injection of additional task types

    @param project_id Project which was just created
    @param project_type_id Type of the project.
    @param user_id User which is interested in the task_types (for I18N)
} -

#---------------------------------------------------------------
# Trans project after update
#---------------------------------------------------------------

ad_proc -public -callback webix::trans::project::after_update {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
} {
    After update (primarily status change) callback for translation projects
} - 

ad_proc -public -callback webix::trans::project::after_update -impl "aa_update_object" {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
} {
    After update (primarily status change) callback for translation projects
} {
    db_dml update_assignment_object "update acs_objects set last_modified = now(), modifying_user = :user_id where object_id = :project_id"
}

ad_proc -public -callback webix::trans::project::after_update -impl "ab_update_cost_cache" {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
} {
    After update (primarily status change) callback for translation projects
} {
    if {$old_project_status_id ne $project_status_id} {
        cog::project::update_cost_cache -project_id $project_id
    }
}

ad_proc -public -callback webix::trans::project::after_update -impl "webix-potential-to-open" {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
} {
    After run if the status changed to open from potential

    Will send out created assignments and setting them to requested
} {
    if {[im_category_is_a $old_project_status_id [im_project_status_potential]] && [im_category_is_a $project_status_id [im_project_status_open]]} {
        set created_assignment_ids [db_list assignments "select assignment_id from im_freelance_assignments fa, im_freelance_packages fp 
            where fp.project_id = :project_id
            and fp.freelance_package_id = fa.freelance_package_id
            and fa.assignment_status_id = 4220"]
        
        foreach assignment_id $created_assignment_ids {
            cog_rest::put::translation_assignments -assignment_id $assignment_id -rest_user_id $user_id -assignment_status_id 4221
        }
    }
}

ad_proc -public -callback webix::trans::project::after_update -impl "webix-open-to-delivered" {
    -project_id:required
    -project_status_id:required
    -old_project_status_id:required
    -user_id:required
} {
    After run if the status changed to delivered from open

    Close all assignments where work was done or we waited for a reaction.
} {
    if {$old_project_status_id eq [im_project_status_open] && $project_status_id eq [im_project_status_delivered]} {
        set assignment_ids [db_list assignments "select assignment_id from im_freelance_assignments fa, im_freelance_packages fp 
            where fp.project_id = :project_id
            and fp.freelance_package_id = fa.freelance_package_id
            and fa.assignment_status_id in (4220, 4221,4222, 4224, 4225,4226,4229)"]
        
        foreach assignment_id $assignment_ids {
            cog_rest::put::translation_assignments -assignment_id $assignment_id -rest_user_id $user_id -assignment_status_id 4231
        }
    }
}


ad_proc -public -callback webix_freelance_packages_after_create {
    {-trans_task_ids:required}
    {-freelance_package_id:required}
} {
    Callback that is executed after freelance packages are generated for trans_task_ids
} -

ad_proc -public -callback webix::assignment::default_filters {
    -project_id:required
    -freelance_package_id:required
    -user_id:required
} {
    Callback to add custom return values
} - 

ad_proc -public -callback webix::assignment::quality_rating_types {
    -assignment_ids:required
    -rest_user_id:required
} {
    Callback to define the quality_type_ids and quality_level_ids
} - 

ad_proc -public -callback webix::packages::after_deadline_calculation {
    -freelance_package_id:required
    -package_deadline:required
} {
    Callback to ammend the deadline after it was auto calculated
} -


#---------------------------------------------------------------
# trans_assignment_after_update
#---------------------------------------------------------------

ad_proc -public -callback webix::assignment::trans_assignment_after_create {
    -assignment_id:required
    -assignment_status_id:required
    -assignment_type_id:required
    -user_id:required
} {
    Callback after an assignment was created. Useful for notifications.
} - 

ad_proc -public -callback webix::assignment::trans_assignment_after_update {
    -assignment_id:required
    -user_id:required
} {
    Callback primarily run to show a change of assignment status
} - 

ad_proc -public -callback webix::assignment::trans_assignment_after_create -impl requested_notifications {
    -assignment_id:required
    -assignment_status_id:required
    -assignment_type_id:required
    -user_id:required
} {
    remind the freelancer about the requested assignment
} {
    switch $assignment_status_id {
        4221 {
            webix::assignments::notify_freelancer -assignment_id $assignment_id
        }
    }    
}

ad_proc -public -callback webix::assignment::trans_assignment_after_create -impl open_project {
    -assignment_id:required
    -assignment_status_id:required
    -assignment_type_id:required
    -user_id:required
} {
    Set the status to requested and notify the freelancer in case this is an open project
} {
    set project_status_id [db_string project "select project_status_id 
        from im_projects p, im_freelance_packages fp, im_freelance_assignments fa
        where fa.assignment_id = :assignment_id
        and fa.freelance_package_id = fp.freelance_package_id
        and fp.project_id = p.project_id"]
    
    if {[im_category_is_a $project_status_id [im_project_status_open]]} {
        cog_rest::put::translation_assignments -assignment_id $assignment_id \
            -rest_user_id $user_id -assignment_status_id [translation_assignment_status_requested]
    }
}

ad_proc -public -callback webix::assignment::trans_assignment_after_update -impl status_change {
    -assignment_id:required
    -user_id:required
} {
    After a status change
    * update_other_requests
    * assignment_remind_freelancer
    * remove overdue notifications
} {
    upvar old_assignment old
    db_1row old_assignment "select * from im_freelance_assignments where assignment_id = :assignment_id"

    switch $assignment_status_id {
        4221 {
            # If the new status_id is requested, remind the freelancer to accept/deny the project
            if {$old(assignment_status_id) ne 4221} {
                webix::assignments::notify_freelancer -assignment_id $assignment_id
            }
        }
        4222 {
            # If the new status_id is accepted, set all others to assign_others status if not already done so
            if {$old(assignment_status_id) ne 4222} {
                # get all assignments with status "requested" for the package_id
                set to_update_status_ids [list 4220 4221] ; # created and requeste

                set to_update_assignment_ids [db_list to_update_assignmente "select assignment_id from im_freelance_assignments 
                    where freelance_package_id = (select freelance_package_id from im_freelance_assignments where assignment_id = :assignment_id)
                    and assignment_status_id in ([template::util::tcl_to_sql_list $to_update_status_ids])"]

                foreach assign_other_assignment_id $to_update_assignment_ids {
                    cog_rest::put::translation_assignments -assignment_id $assign_other_assignment_id -assignment_status_id 4228 -rest_user_id $user_id
                }

                # Remove notifications for request outstanding
                lappend to_update_assignment_ids $assignment_id
                foreach notification_id [db_list notifications "select notification_id from webix_notifications 
                    where context_id in ([template::util::tcl_to_sql_list $to_update_assignment_ids]) 
                    and notification_type_id = [webix_notification_type_assignment_request_out]"] {
                    webix::notification::delete -notification_id $notification_id
                }
            }
        }
        4223 {
            foreach notification_id [db_list notifications "select notification_id from webix_notifications 
                where context_id = :assignment_id
                and notification_type_id in ([webix_notification_type_assignment_request_out], [webix_notification_type_assignment_accepted])"] {
                webix::notification::delete -notification_id $notification_id
            }
        }
        4225 {
            # Remove overdue notifications
            foreach notification_id [db_list notifications "select notification_id from webix_notifications where context_id = :assignment_id and notification_type_id in ([webix_notification_type_assignment_overdue],[webix_notification_type_assignment_accepted])"] {
                webix::notification::delete -notification_id $notification_id
            }
        }
    }
    db_dml update_assignment_object "update acs_objects set last_modified = now(), modifying_user = :user_id where object_id = :assignment_id"
}

ad_proc -public -callback cog_rest::cr_file_after_upload -impl package_file_upload {
    -file_item_id
    -file_revision_id
    -context_id
    -context_type
    -user_id
} {
    Callback after a file was uploaded to a package or assignment

    @param file_item_id ItemId of the file we upload
    @param file_revision_id Revision of the file we uploaded
    @param context_id Context which was Provided
    @param context_type Object Type of the context
    @param user_id User who uploaded the package file
} {
	set internal_company_name [im_name_from_id [im_company_freelance]]
	db_1row revision_info "select ci.name, cr.description from cr_items ci, cr_revisions cr 
	where ci.item_id = cr.item_id 
	and cr.revision_id = :file_revision_id"

    set context_name [im_name_from_id $context_id]

    switch $context_type {
        im_freelance_assignment {
            # This must be a return file from the freelancer which is associated with the assignment.
            db_1row assignment_info "select assignment_status_id, assignee_id from im_freelance_assignments where assignment_id = :context_id"
            permission::grant -party_id [im_profile_employees] -object_id $context_id -privilege "read"
        }
        im_freelance_package - content_folder {
            # This must be a file which is uploaded from the PM for freelancers to work on.
            if {$context_type eq "im_freelance_package"} {
                set sql "select assignment_id,p.project_id, project_nr, assignment_status_id, 
                assignee_id, project_lead_id
                from im_freelance_assignments fa, im_freelance_packages fp, im_projects p
                where fp.freelance_package_id = :context_id
                and fp.freelance_package_id = fa.freelance_package_id
                and p.project_id = fp.project_id"
            } else {
                set project_id [intranet_fs::get_project_from_folder_id -folder_id $context_id]
                set sql "select assignment_id,p.project_id, project_nr, assignment_status_id, 
                assignee_id, project_lead_id
                from im_freelance_assignments fa, im_freelance_packages fp, im_projects p
                where fp.project_id = :project_id
                and fp.freelance_package_id = fa.freelance_package_id
                and p.project_id = fp.project_id"
            }

            # Check if we have an assignment and set it to package available if it is still in accepted status
            db_foreach assignments $sql {            

                set locale [lang::user::locale -user_id $assignee_id]
                set salutation_pretty [im_invoice_salutation -person_id $assignee_id]
                set notify_assignee_p 1
                switch $assignment_status_id {
                    4221 {
                        set notify_assignee_p 0
                        permission::grant -party_id $assignee_id -object_id $file_item_id -privilege "read"
                    }
                    4222 - 4224 - 4227 {
                        # Accepted - add permissions to assignee
                        permission::grant -party_id $assignee_id -object_id $file_item_id -privilege "read"
                        set body "[lang::message::lookup $locale webix-portal.lt_file_uploaded_message_body]"
                    }
                    4225 - 4226 - 4231 {
                        # Work has been delivered already. Add permission and notify the PM
                        permission::grant -party_id $assignee_id -object_id $file_item_id -privilege "read"

                        if {$context_type eq "im_freelance_package"} {
                            # New package => Reopen assignment.
                            webix::assignments::change_status -assignment_id $assignment_id -assignment_status_id [translation_assignment_status_in_progress]

                            set body "[lang::message::lookup $locale webix-portal.lt_assignment_reopened]"
                        } else {
                            set body "[lang::message::lookup $locale webix-portal.lt_file_uploaded_message_body]"
                        }
                    }
                    default {
                        # Denied, Deleted - do nothing
                        set notify_assignee_p 0
                        cog_log Notice "Assignment in status $assignment_status_id"
                    }
                }

                if {$notify_assignee_p} {
		
                    set notification_id [webix::notification::create \
                        -notification_status_id [webix_notification_status_default] \
                        -notification_type_id [webix_notification_type_file_uploaded] \
                        -context_id $assignment_id \
                        -project_id $project_id \
                        -recipient_id $assignee_id \
                        -message "[_ webix-portal.file_uploaded_for_project]" \
                        -creation_user_id $user_id]

                    set subject "[lang::message::lookup $locale webix-portal.lt_file_uploaded_message_subject]"

                    webix::notification::action_create \
                        -notification_id $notification_id \
                        -action_type_id [webix_notification_action_type_download_files] \
                        -action_object_id $context_id

                    webix::notification::action_create \
                        -notification_id $notification_id \
                        -action_type_id [webix_notification_action_type_view_comments] \
                        -action_object_id $file_item_id \
                        -action_help "[_ webix-portal.view_comment]" \
                        -action_text $description

                    intranet_chilkat::send_mail  -to_party_ids $assignee_id  -from_party_id $project_lead_id  -subject $subject  -body $body  -no_callback
                }
            }
        }
    }
}

ad_proc -public -callback im_openoffice_invoice_pdf_after_create -impl webix_quote_contact_again_date {
	{-invoice_id:required}
	{-preview_p "0"}
	{-invoice_nr:required}
	{-invoice_pdf_revision_id ""}
} {
	Set the contact again date if we download or send the financial document and it does not exist yet.
} {

	set cost_type_id [db_string select_cost_type "select cost_type_id from im_costs where cost_id = :invoice_id" -default ""]
	if {$cost_type_id eq 3702} {
        if {!$preview_p} {
            set project_id [db_string project "select project_id from im_costs where cost_id = :invoice_id" -default ""]
            set days_in_future [parameter::get_from_package_key -package_key "webix-portal" -parameter "ContactAgainDays"]
            db_dml update_date "update im_projects set contact_again_date = now() + interval '$days_in_future days' where project_id = :project_id"
        }
	}
}

ad_proc -public -callback webix::trans::project::after_create -impl description_as_note {
	-project_id:required
	-user_id:required
} {
    Adds the description as a note in case it isn't displayed somewhere else.
} {
	db_1row project_info "select description from im_projects where project_id = :project_id"
    cog::note::add \
        -user_id $user_id \
        -note $description \
        -object_id $project_id \
        -note_type_id [im_note_type_project_info]
}

ad_proc -callback im_invoice_after_update -impl webix_delete {
    -object_id:required
    -status_id:required
    -type_id:required
} {
    Delete invoice from assignements
} {
    if {$status_id eq [im_cost_status_deleted] || $status_id eq [im_cost_status_rejected]} {
        set assignment_id [db_string assignment "select assignment_id from im_freelance_assignments where purchase_order_id = :object_id" -default ""] 
        if {$assignment_id ne ""} {
            cog_rest::put::translation_assignments -assignment_id $assignment_id -purchase_order_id "" -rest_user_id [auth::get_user_id]
        }
    }
}


ad_proc -public -callback im_invoice_after_update -impl webix_update_accepted_quote_open_project {
    -object_id
    -status_id
    -type_id
} {
    Checks for an accepted quote if the project is still potential. If yes, set the project to open.
} {
    if {$type_id eq [im_cost_type_quote] && $status_id eq [im_cost_status_accepted]} {
        set project_id [db_string project "select project_id from im_costs where cost_id = :object_id" -default ""]
        if {$project_id ne ""} {
            set project_status_id [db_string project_status "select project_status_id from im_projects where project_id = :project_id" -default ""]
            if {[lsearch [im_sub_categories [im_project_status_potential]] $project_status_id]>-1} {
                cog_rest::put::trans_project -project_id $project_id -rest_user_id [auth::get_user_id] -project_status_id [im_project_status_open]
            }
        }
    }
}

ad_proc -public -callback cog_rest::dynamic::get_object -impl webix_trans_projects {
    -object_id
    -object_types
} {
    Add language attributes to translation projects
} {
    if {[lsearch $object_types "im_project"]>-1} {
        # Check if we have target language ids
        upvar key_value_list key_value_list
        set target_language_ids [db_list target_language_ids "select language_id from im_target_languages where project_id = :object_id"]
        if {[llength $target_language_ids]>0} {
            set display_values ""
            set id "target_language_id"
            set value [join $target_language_ids ","]
            set display_values [list]
            foreach target_language_id $target_language_ids {
                lappend display_values [im_name_from_id $target_language_id]
            }
            set display_value [join $display_values ","]
            lappend key_value_list [cog_rest::json_object -proc_name "cog_rest::get::dynamic_object" -object_class "id_value"]
        }

        # Check if we have Business Sector ids (Themengebiet)
        set business_sector_ids [db_list business_sector_ids "select skill_id from im_object_freelance_skill_map where object_id = :object_id and skill_type_id = 2024"]
        if {[llength $business_sector_ids]>0} {
            set display_values ""
            set id "business_sector_id"
            set value [join $business_sector_ids ","]
            set display_value [list]
            foreach business_sector_id $business_sector_ids {
                lappend display_values [im_name_from_id $business_sector_id]
            }
            set display_value [join $display_values ","]
            lappend key_value_list [cog_rest::json_object -proc_name "cog_rest::get::dynamic_object" -object_class "id_value"]
        }
    }
}

ad_proc -public -callback cog_rest::dynamic::put_object -impl webix_trans_projects {
    -object_id
    -object_types
} {
    Add language attributes to translation projects
} {
    if {[lsearch $object_types "im_project"]>-1} {
        # Create 'sql' expression to get all Target Languages from skills table
        set language_from_skills_sql "select  skill_id
	        from	im_object_freelance_skill_map
	        where   object_id = :object_id
		    and skill_type_id = [im_freelance_skill_type_target_language]
	    "
        # Delete existing ones
        db_dml delete_target_languages "delete from im_target_languages where project_id = :object_id"

        # Insert new ones
        db_foreach update_tl_table $language_from_skills_sql {
            set insert_sql "insert into im_target_languages(project_id, language_id) values (:object_id, :skill_id)"
            db_dml insert_new_target_languages $insert_sql
        }
    }
}