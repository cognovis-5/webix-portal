# /packages/sencha-portal-rest-procs

ad_library {
    Rest Procedures for the sencha-portal package - project related
    @author michaldrn@wp.pl
}

namespace eval cog_rest::json_object {
    ad_proc -public user_for_switching {} {
        @return user object person User which we want to switch into 
        @return locale string locale of the user
	    @return profiles json_array group array of groups the user is a member of.
    } - 
    ad_proc user_project {} {
        @return project named_id Project to look at 
        @return project_nr string ProjectNR for the project
    } -
}

namespace eval cog_rest::get {

    ad_proc -public project_members {
        -rest_user_id:required
        -project_id:required
    } {
        Returns the project_members for a project. Extends the biz object members with freelancers

        @param project_id object im_project::read Project for which we want to see the project_members

        @return members json_array biz_object_member Array of Members for this project
    } {
        set members [cog_rest::helper::json_array_to_object_list -json_array [cog_rest::get::biz_object_members -object_id $project_id -rest_user_id $rest_user_id]]

        db_foreach assignee {select assignee_id
            from im_freelance_assignments fa, im_freelance_packages fp 
            where fp.freelance_package_id = fa.freelance_package_id
            and fp.project_id = :project_id
            and fa.assignment_status_id in (4222,4224,4225,42267,4227,4231)
        } {
            set object_id $project_id
            set rel_id ""
            set percentage ""
            set role_id 1310 ; # Freelancer biz object
            set member [ cog_rest::helper::json_array_to_object -json_array [cog_rest::get::user -user_ids $assignee_id -rest_user_id $rest_user_id]]
            lappend members [cog_rest::json_object]
        }

        return [cog_rest::json_response]
    }

    ad_proc -public user_projects {
        -rest_user_id:required
        {-user_id ""}
    } {
        Returns all projects of which user is a member

        @param user_id object im_person::read User for whom we want to get the list of his active project

        @return user_projects json_array user_project
    } {

        # Default user is the one who make API call
        if {$user_id eq ""} {
            set user_id $rest_user_id
        }

        # We only want 'active' projects, which means the ones with OPEN or POTENTIAL status (+ subcategories)
        set statuses_categories_ids [list]
        foreach main_cat [list [im_project_status_potential] [im_project_status_open]] {
            lappend statuses_categories_ids $main_cat
            foreach sub_cat [im_sub_categories $main_cat] {
                lappend statuses_categories_ids $sub_cat
            }
        }
        set where_clauses [list]
        lappend where_clauses "project_status_id in ([template::util::tcl_to_sql_list $statuses_categories_ids])"

        # Make sure to exclude projects which are tasks
        set task_type_categories_ids [list [im_project_type_task]]
        lappend where_clauses "project_type_id not in ([template::util::tcl_to_sql_list $task_type_categories_ids])"

        # First we get only project_ids
        set user_projects [list]
        set user_projects_ids [db_list user_projects "SELECT rels.object_id_one as object_id FROM acs_rels rels WHERE rels.object_id_two =:user_id 
            AND (SELECT count(*) from im_projects WHERE project_id = rels.object_id_one AND [join $where_clauses " AND "] ) > 0"]
        # We continue only if user has at least one project
        if {[llength $user_projects_ids] > 0} {
            # Now we can build query to get project_id, project_name and project_nr. We do not need anything else in here
            set projects_sql "SELECT project_id, project_name, project_nr FROM im_projects where project_id in ([template::util::tcl_to_sql_list $user_projects_ids])"
            db_foreach user_project $projects_sql {
                lappend user_projects [cog_rest::json_object]
            }
        }

        return [cog_rest::json_response]
    }


    ad_proc -public user_switches {
        -rest_user_id:required
        {-real_user_id ""}
        { -limit 30}

    } {
        Handler for GET call for an PO Admin to get possible users wcich Admin can fake

        @param real_user_id integer user_id of real user (admin user_id)
        @param limit integer how many should we display
        @return users json_array user_for_switching Array of possible 'to switch' users

    } {

        set users [list]

        # Prepare variable aliases, just for better code readability
        set faked_user_id $rest_user_id
        set admin_user_id $real_user_id

        # We need to make sure if admin is really an admin
        set admin_p [im_is_user_site_wide_or_intranet_admin $admin_user_id]

        if {$admin_p} {

            set users_sql "(select user_id
                from    users u, persons pe, group_member_map g
                where   u.user_id = pe.person_id and
                    u.user_id in (
                        SELECT  o.object_id
                        FROM    acs_objects o,
                                group_member_map m,
                                membership_rels mr
                        WHERE   m.member_id = o.object_id AND
                                m.group_id = acs__magic_object_id('registered_users'::character varying) AND
                                m.rel_id = mr.rel_id AND
                                m.container_id = m.group_id AND
                                m.rel_type::text = 'membership_rel'::text AND
                                mr.member_state = 'approved'

                    )
                    and u.user_id = g.member_id 
                    and g.group_id in (463,461,471,465,459)
                    and last_visit is not null

            ) order by last_visit desc, last_name desc limit :limit"

            db_foreach user $users_sql {
                set user_name [im_name_from_id $user_id]
                set locale [lang::user::locale -user_id $user_id]
                set profiles [cog_rest::user_profile_objects -user_id $user_id]
                lappend users [cog_rest::json_object]
            }

        }

        return [cog_rest::json_response]

    }


    ad_proc -public company_new_project_defaults {
        -customer_id:required
        -rest_user_id:required
    } {
        Get the most often used values for the creation of a new project

        @param customer_id integer company_id of customer

        @return  data object of customer defaults

        @return_data project_type_id integer default project type for that customer
        @return_data source_language_id integer default source language for that customer
        @return_data target_language_id integer default target language for that customer
        @return_data subject_area_id integer default subject area for that customer
        @return_data final_company_id integer default final company for that customer
        @return_data uom_id integer default unit of measure for that customer
        @return_data complexity_type_id integer default complexity_type_id for that customer
    } {

        # Get the most often used project type for the customer
        set project_type_id [db_string project_type "select project_type_id from (select project_type_id, count(project_type_id) as count from im_projects where company_id = :customer_id group by project_type_id) c order by count desc limit 1" -default ""]
        set source_language_id [db_string source_langauge "select source_language_id from (select source_language_id, count(source_language_id) as count from im_projects where company_id = :customer_id group by source_language_id) c order by count desc limit 1" -default ""]
        set subject_area_id [db_string subject_area "select subject_area_id from (select subject_area_id, count(subject_area_id) as count from im_projects where company_id = :customer_id group by subject_area_id) c order by count desc limit 1" -default ""]
        set final_company_id [db_string final_company "select final_company_id from (select final_company_id, count(final_company_id) as count from im_projects where company_id = :customer_id group by final_company_id) c order by count desc limit 1" -default ""]
        set target_language_id [db_string target_language "select language_id from (select language_id, count(language_id) as count from im_target_languages tl, im_projects p where p.company_id = :customer_id and p.project_id = tl.project_id group by language_id) c order by count desc limit 1" -default ""]
        set uom_id [db_string uom "select item_uom_id from (select count(item_uom_id) as count, item_uom_id from im_invoice_items ii, im_costs c where ii.invoice_id = c.cost_id and c.customer_id = :customer_id group by item_uom_id order by count desc) s limit 1" -default ""]
        set complexity_type_id [db_string complexity_type "select complexity_type_id from (select complexity_type_id, count(complexity_type_id) as count from im_projects where company_id = :customer_id group by complexity_type_id) c order by count desc limit 1" -default ""]
                
        return [cog_rest::json_object]

    }

    ad_proc -public basic_project_info {
        -project_id
        { -rest_user_id 0 }
    } {
        Handler for GET rest call which return basic project info for portals.
        This is a replacement for 'portlets' used in old interface.
        Currently used in FreelancerPortal and CustomerPortal, PM App needs more details in that section.

        This procedure decides which values to display.

        @param project_id integer ProjectId for which to show the project_info

        @return project_info

        @return_project_info name string Name of the variable
        @return_project_info value string Value of the info field
        @return_project_info label string Translated label of the variable

    } {

        set project_info [list]

        # check that the rest_user has permission to see the project
        set view_p 0
        # check for assignments

        if {[im_user_is_freelance_p $rest_user_id]} {
            set freelancers_on_project [im_assignment_freelancers_working_ids -project_id $project_id]

            if {[lsearch $freelancers_on_project $rest_user_id]>-1} {
                set view_p 1
            }
        } 

        # Check if the customer is a member of the company of the project
        if {[im_user_is_customer_p $rest_user_id]} {
            set company_id [db_string company "select company_id from im_projects where project_id = :project_id"]
            if {[im_biz_object_member_p $rest_user_id $company_id]} {
                set view_p 1
            }
        }

        if {[im_user_is_admin_p $rest_user_id]} {
            set view_p 1
        }

        
        if {$view_p eq 1} {
            set locale [lang::user::locale -user_id $rest_user_id]

            set output_variables [list \
                [list project_nr "[lang::message::lookup $locale sencha-portal.project_nr]"] \
                [list project_name "[lang::message::lookup $locale sencha-freelance-translation.Project_Name]"] \
                [list project_lead_name "[lang::message::lookup $locale sencha-freelance-translation.Project_Manager]"] \
                [list language "[lang::message::lookup $locale sencha-freelance-translation.Language]"] \
                [list start_date "[lang::message::lookup $locale sencha-assignment.start_date]"] \
                [list project_status_id "project status"] \
                [list project_lead_id "project lead"] \
                [list project_status "[lang::message::lookup $locale intranet-sencha-tables.project_status]"] \
                [list number_of_quotes "number of quotes"]
            ] 
            
            # Now we need to figure out if we want to display accept / reject buttons
            # We need to get project financial documents and set number_of_quotes

            set financial_documents [cog_rest::get::invoice -rest_user_id $rest_user_id -project_id $project_id -cost_status_id "" -cost_type_id [im_cost_type_quote]]
            set number_of_quotes [llength $financial_documents]

            set num_assignments 0
            set skill_2006 ""
            set skill_2024 ""
            set skill_2014 ""
            set skill_2000 ""
            set skill_2002 ""

            set currency "EUR"

            set po_amount 0
            set total_amount 0
            set total_amount_pretty 0

            set success true
            set project_data ""
            db_1row project_info "select aux_string1, project_nr, project_name, project_status_id, im_name_from_id(project_status_id) as project_status, company_project_nr, project_lead_id, im_name_from_id(project_lead_id) as project_lead_name, company_contact_id, im_name_from_id(company_contact_id) as company_contact_name,end_date, start_date,source_language_id from im_projects, im_categories where project_id = :project_id and project_type_id = category_id"

            # Active docs
            # Do not include financial documents with status 3818 and 3812 with subcategories
            
            set not_in_cost_status_id [im_sub_categories [im_cost_status_deleted]]
            lappend not_in_cost_status_id [im_cost_status_rejected]
            
            set not_in_status_query " and cost_status_id not in([template::util::tcl_to_sql_list $not_in_cost_status_id])"

            db_foreach skills_for_object "select skill_type_id, skill_id from im_object_freelance_skill_map, im_categories where object_id = :project_id and category_id = skill_type_id order by sort_order " {
                set skill_$skill_type_id [im_name_from_id $skill_id]
            }

            set start_date [lc_time_fmt $start_date "%q"]
            set end_date [lc_time_fmt $end_date "%q %X"]
            
            if {[im_user_is_freelance_p $rest_user_id]} {
                set assignment_target_languages [list]
                # Now we must loop thru assignments to get num_assignments and total_amount
                set assignments_sql "select fa.assignment_id, fp.freelance_package_id, uom_id, im_name_from_id(uom_id) as uom, assignment_units, rate, fa.purchase_order_id from im_freelance_assignments fa, im_freelance_packages fp where fa.freelance_package_id = fp.freelance_package_id and fp.project_id =:project_id and fa.assignee_id =:rest_user_id and fa.assignment_status_id <> 4230"
                db_foreach assignment $assignments_sql {
                    set po_sum [db_string total "select amount, currency from im_costs where cost_id = :purchase_order_id" -default ""]
                    if {$po_sum ne ""} {
                        set total_amount  [expr $total_amount + $po_sum]
                        set po_sum [format "%.2f" $po_sum]
                        #  set po_sum [expr round(100 * $po_sum) / 100]
                    set po_amount "$po_sum $currency"
                    } else {
                        if {$uom_id eq [im_uom_unit]} {
                            set total_amount [expr $total_amount + $rate]
                        } else {
                            set total_amount [expr $total_amount + ($assignment_units * $rate)]
                        }
                        set formatted_rate [im_report_format_number $rate]
                        set formatted_total_units_rate [im_report_format_number [expr $assignment_units * $rate]]
                        set po_amount "$assignment_units $uom <br />$formatted_rate $currency / $uom <br />($formatted_total_units_rate $currency)"
                    }
                    set total_amount_pretty "[format "%.2f" $total_amount] $currency"
                    
                    set target_language_name_sql "select distinct im_name_from_id(target_language_id) as target_language_name from im_freelance_assignments fa, im_trans_tasks tt, im_freelance_packages_trans_tasks fpf where fa.freelance_package_id = fpf.freelance_package_id and fpf.trans_task_id = tt.task_id and fa.assignment_id =:assignment_id" 
                    db_foreach target_langs $target_language_name_sql {
                        lappend assignment_target_languages $target_language_name
                    }
                    
    
                    incr num_assignments
                }
            }

            if {[im_user_is_freelance_p $rest_user_id]} {
                lappend output_variables [list skill_2006 "[lang::message::lookup $locale sencha-freelance-translation.CAT]"] 
                lappend output_variables [list skill_2024 "[lang::message::lookup $locale sencha-freelance-translation.Subject_Area]"]
                lappend output_variables [list skill_2024 "[lang::message::lookup $locale sencha-freelance-translation.Business_Area]"]
                lappend output_variables [list num_assignments "[lang::message::lookup $locale sencha-freelance-translation.Assignments]"]
                lappend output_variables [list total_amount_pretty "[lang::message::lookup $locale sencha-freelance-translation.Honorar_Gesamt]"]
                #set language "$skill_2000 => $skill_2002"
                set language "$skill_2000 => [join $assignment_target_languages ","]"
            }

            if {[im_user_is_customer_p $rest_user_id]} {
                lappend output_variables [list company_contact_id "[lang::message::lookup $locale sencha-portal.client_contact]"]
                lappend output_variables [list company_contact_name "[lang::message::lookup $locale sencha-portal.client_contact]"]
                lappend output_variables [list end_date "[lang::message::lookup $locale sencha-freelance-translation.Project_Deadline]"]
                lappend output_variables [list company_project_nr "[lang::message::lookup $locale sencha-portal.company_project_nr]"]
                lappend output_variables [list total_amount_pretty_customer "[lang::message::lookup $locale sencha-portal.gesamtvolumen]"]
                set total_amount_pretty_customer [db_string get_customer_total_amount "select sum(amount) from im_costs where project_id =:project_id and cost_type_id = [im_cost_type_quote] $not_in_status_query"]            
                # Get the languages and target languages
                set target_languages [db_list targets "select im_name_from_id(language_id) from im_target_languages where project_id = :project_id"]
                if {$source_language_id ne ""} {
                    set language "[im_name_from_id $source_language_id] => [join $target_languages ","]"
                } else {
                    set language ""
                }
            }

            set project_info [list]
            foreach var $output_variables {
                catch {
                    set label [lindex $var 1]
                    set name [lindex $var 0]
                    eval "set value $$name"

                    lappend project_info [cog_rest::json_object]
                }
            }

        }

            return [cog_rest::return_array]

    }

}

namespace eval cog_rest::post {

    ad_proc -public project_take_over {
        -project_id:required
        -rest_user_id:required
        { -new_project_lead_id ""}
    } {
        it literally means updating im_projects project_lead_id.
        If no new_project_lead_id is provided, we use rest_user_id as default.

        @param project_id object im_project::write project in which we want to do the 'take_over'
        @param new_project_lead_id object user::read user who now will became new pm (new project_lead_id in `im_projects`)

        @return errors json_array error Array of errors found

    } {

        set errors [list]

        if {$new_project_lead_id eq ""} {
            set new_project_lead_id $rest_user_id
        }

        # Update project_lead_id
        db_dml update_project_lead_id "update im_projects set project_lead_id =:new_project_lead_id where project_id=:project_id"

        # Add user as project member, only in case he is still not a member
        set member_role_id [im_biz_object_role_full_member]
        set project_member_ids [im_biz_object_member_ids $project_id]
        if {[lsearch $project_member_ids $new_project_lead_id]<0} {
            cog::callback::invoke im_before_member_add -user_id $new_project_lead_id -object_id $project_id
            im_biz_object_add_role $new_project_lead_id $project_id $member_role_id
            cog::callback::invoke -object_id $project_id -action "after_update"
        } 
    
        return [cog_rest::json_response]

    }
    
}

