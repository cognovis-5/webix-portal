
ad_library {
    APM callback procedures.

    @creation-date 2021-06-21
    @author malte.sussdorff@cognovis.de
}

namespace eval webix-portal::apm {}

ad_proc -public webix-portal::apm::after_upgrade {
    {-from_version_name:required}
    {-to_version_name:required}
} {
    After upgrade callback.
} {
    apm_upgrade_logic \
        -from_version_name $from_version_name \
        -to_version_name $to_version_name \
        -spec {
            5.0.0.4.1 5.0.0.4.2 {
                # Grant permission to all freelancer on their own assignments
                # Grant permissions on project folders
                db_foreach assignment "select project_id, assignee_id, assignment_id, fa.freelance_package_id 
                    from im_freelance_assignments fa, im_freelance_packages fp where fp.freelance_package_id = fa.freelance_package_id" {
                    set project_folder_id [intranet_fs::get_project_folder_id -project_id $project_id]
                    if {$project_folder_id ne ""} {
                        permission::grant -party_id $assignee_id -object_id $project_folder_id -privilege "read"
                    }
                    permission::grant -party_id $assignee_id -object_id $freelance_package_id -privilege "read"
                    permission::grant -party_id $assignee_id -object_id $assignment_id -privilege "read"
                }
            }
            5.0.0.4.4 5.0.0.4.5 {
                db_foreach quote_in_potential "select p.project_id, cost_id, to_char(effective_date,'YYYY-MM-DD') as effective_date from im_costs c, im_projects p
                    where p.project_id = c.project_id
                    and c.cost_type_id = 3702
                    and p.project_status_id in ([template::util::tcl_to_sql_list [im_sub_categories 71]])" {
                    set days_in_future [parameter::get_from_package_key -package_key "webix-portal" -parameter "ContactAgainDays" -default 7]
                    if {![db_column_exists im_projects contact_again_date]} {
                        db_dml add_contact_again "alter table im_projects add column contact_again_date timestamptz" 
                    }
                    db_dml update_date "update im_projects set contact_again_date = to_date(:effective_date,'YYYY-MM-DD') + interval '$days_in_future days' where project_id = :project_id and contact_again_date is null"
                }
            }
            5.0.0.4.5 5.0.0.5.0 {
                db_foreach project "select project_id, description, project_lead_id from im_projects where description is not null" {
                    if {[catch {set note [template::util::richtext::get_property html_value $description]}]} {
                        set note ""
                    }
                    regsub -all {\n} $note {<br />} note
                    if {$note ne ""} {
                        cog::note::add \
                            -note $note\
                            -user_id $project_lead_id\
                            -object_id $project_id\
                            -note_type_id [im_note_type_project_info]
                    }
                }
            }
            5.0.0.5.0 5.0.0.5.1 {
                db_foreach package "select freelance_package_id, target_language_id from 
                    im_freelance_packages_trans_tasks fptt, im_trans_tasks tt
                    where tt.task_id = fptt.trans_task_id
                    group by freelance_package_id, target_language_id
                    order by freelance_package_id" {
                    db_dml update_package "update im_freelance_packages set target_language_id = :target_language_id where freelance_package_id = :freelance_package_id"
                }

                set to_delete [db_list empty_package "select freelance_package_id from im_freelance_packages where target_language_id is null 
                    and freelance_package_id not in (select freelance_package_id from im_freelance_assignments)"]
                
                if {[llength $to_delete]>0} {
                    cog_rest::delete::packages -package_ids $to_delete
                }
            }
            5.0.0.6.2 5.0.0.6.3 {
                db_foreach purchase_order "select item_id, assignment_id from im_invoice_items i, im_freelance_assignments fa 
                    where i.invoice_id = fa.purchase_order_id 
                    and i.item_units >0" {
                        db_dml update_invoice_item "update im_invoice_items set context_id = :assignment_id where item_id = :item_id"
                    }
            }
            5.0.0.6.8 5.0.0.6.9 {
                db_foreach assignment "select assignment_id, assignee_id
                    from im_freelance_assignments
                " {
                    permission::grant -party_id $assignee_id -object_id $assignment_id -privilege "write"
                }
            }
            5.0.1.2.3 5.0.1.3.0 {
                set portlet_ids [list]
                lappend portlet_ids [webix::portlet::create -page_url "user-details" -sort_order 5 -location "left" -object_type "person" -portlet_type_id 28507 -portlet_name "User Basic Info"]
                lappend portlet_ids [webix::portlet::create -page_url "user-details" -sort_order 25 -location "left" -object_type "person" -portlet_type_id 28509 -portlet_name "User Projects"]
                lappend portlet_ids [webix::portlet::create -page_url "user-details" -sort_order 30 -location "right" -object_type "person" -portlet_type_id 28506 -portlet_name "Main Translators"]
                lappend portlet_ids [webix::portlet::create -page_url "user-details" -sort_order 10 -location "right" -object_type "person" -portlet_type_id 28508 -portlet_name "User Notes"]
                lappend portlet_ids [webix::portlet::create -page_url "user-details" -sort_order 15 -location "left" -object_type "person" -portlet_type_id 28504 -portlet_name "Messages"]
                lappend portlet_ids [webix::portlet::create -page_url "user-details" -sort_order 20 -location "right" -object_type "person" -portlet_type_id 28510 -portlet_name "Skills"]
                foreach portlet_id $portlet_ids {
                    foreach party_id [list 463 467 471] {
                        permission::grant -object_id $portlet_id -party_id $party_id -privilege "read"
                    }
                }
            }
            5.0.1.3.0 5.0.1.3.1 {
                set portlet_ids [list]

                # Company portlets
                lappend portlet_ids [webix::portlet::create -page_url "company-details" -sort_order 5 -location "left" -object_type "im_company" -portlet_type_id 28500 -portlet_name "Company Basic Info"]
                lappend portlet_ids [webix::portlet::create -page_url "company-details" -sort_order 15 -location "left" -object_type "im_company" -portlet_type_id 28504 -portlet_name "Messages"]
                lappend portlet_ids [webix::portlet::create -page_url "company-details" -sort_order 20 -location "left" -object_type "im_company" -portlet_type_id 28503 -portlet_name "Company Contacts"]                
                lappend portlet_ids [webix::portlet::create -page_url "company-details" -sort_order 25 -location "left" -object_type "im_company" -portlet_type_id 28501 -portlet_name "Company Projects"]

                lappend portlet_ids [webix::portlet::create -page_url "company-details" -sort_order 5 -location "right" -object_type "im_company" -portlet_type_id 28502 -portlet_name "Company Prices"]
                lappend portlet_ids [webix::portlet::create -page_url "company-details" -sort_order 10 -location "right" -object_type "im_company" -portlet_type_id 28508 -portlet_name "Company Notes"]
                lappend portlet_ids [webix::portlet::create -page_url "company-details" -sort_order 20 -location "right" -object_type "im_company" -portlet_type_id 28504 -portlet_name "Messages"]
                lappend portlet_ids [webix::portlet::create -page_url "company-details" -sort_order 30 -location "right" -object_type "im_company" -portlet_type_id 28506 -portlet_name "Main Translators"]

                # Project portlets
                im_category_add -category_id 28511 -category "ProjectBasicInfoPortlet" -category_type "Webix Portlet Type"
                im_category_add -category_id 28512 -category "ProjectTimesheetPorlet" -category_type "Webix Portlet Type"
                im_category_add -category_id 28513 -category "ProjectSubProjects" -category_type "Webix Portlet Type"
                im_category_add -category_id 28514 -category "ProjectTeamPortlet" -category_type "Webix Portlet Type"

                lappend portlet_ids [webix::portlet::create -page_url "project-details" -sort_order 5 -location "left" -object_type "im_project" -portlet_type_id 28511 -portlet_name "Project Basic Info"]
                lappend portlet_ids [webix::portlet::create -page_url "project-details" -sort_order 10 -location "left" -object_type "im_project" -portlet_type_id 28514 -portlet_name "Project Team"]
                lappend portlet_ids [webix::portlet::create -page_url "project-details" -sort_order 15 -location "left" -object_type "im_project" -portlet_type_id 28508 -portlet_name "Project Notes"]
                
                
                lappend portlet_ids [webix::portlet::create -page_url "project-details" -sort_order 5 -location "right" -object_type "im_project" -portlet_type_id 28512 -portlet_name "Project Timesheet"]
                lappend portlet_ids [webix::portlet::create -page_url "project-details" -sort_order 10 -location "right" -object_type "im_project" -portlet_type_id 28513 -portlet_name "Sub Projects"]
                lappend portlet_ids [webix::portlet::create -page_url "project-details" -sort_order 20 -location "right" -object_type "im_project" -portlet_type_id 28504 -portlet_name "Messages"]
                
                # Account Info
                im_category_add -category_id 28515 -category "WorkCalendar" -category_type "Webix Portlet Type"
                im_category_add -category_id 28516 -category "VacationsPortlet" -category_type "Webix Portlet Type"
                lappend portlet_ids [webix::portlet::create -page_url "account-info" -sort_order 5 -location "left" -object_type "user" -portlet_type_id 28515 -portlet_name "Work Calendar"]
                lappend portlet_ids [webix::portlet::create -page_url "account-info" -sort_order 5 -location "right" -object_type "user" -portlet_type_id 28512 -portlet_name "User Timesheet"]
                lappend portlet_ids [webix::portlet::create -page_url "account-info" -sort_order 10 -location "right" -object_type "user" -portlet_type_id 28516 -portlet_name "User Vacation"]


                foreach portlet_id $portlet_ids {
                    foreach party_id [list 463 467 471] {
                        permission::grant -object_id $portlet_id -party_id $party_id -privilege "read"
                    }
                }
            }
            5.0.1.4.9 5.0.1.5.0 {
                # Migrate assignment comments
                db_foreach assignment_comment "select assignment_id, assignment_comment, creation_user from im_freelance_assignments, acs_objects where object_id = assignment_id and assignment_comment is not null" {
                    cog::note::add -user_id $creation_user -note $assignment_comment -object_id $assignment_id -note_type_id [im_note_type_delivery_info] -note_status_id [im_note_status_active]
                }
            }
        }
}

ad_proc -public webix-portal::apm::after_install {} {
    Run through upgrades necessary for webix-portal to work
} {

    set core_upgrade_dir "[acs_root_dir]/packages/webix-portal/sql/postgresql/upgrade"
    foreach dir [lsort [glob -type f -nocomplain "$core_upgrade_dir/upgrade-?.?.?.?.?-?.?.?.?.?.sql"]] {
        # Skip upgrade scripts from 3.0.x
        if {[regexp {upgrade-3\.0.*\.sql} $dir match path]} { continue }

        # Add the "/packages/..." part to hash-array for fast comparison.
        if {[regexp {(/packages.*)} $dir match path]} {
            set fs_files($path) $path
        }
    }


    # --------------------------------------------------------------
    # Get the upgrade scripts that were executed
    set sql "
        select  distinct l.log_key
        from    acs_logs l
        order by log_key
    "
    db_foreach db_files $sql {
        # Add the "/packages/..." part to hash-array for fast comparison.
        if {[regexp {(/packages.*)} $log_key match path]} {
            set db_files($path) $path
        }
    }

    foreach file [lsort [array names fs_files]] {
         if {![info exists db_files($file)]} {
            catch {db_source_sql_file -callback apm_ns_write_callback $file}
        }
    }

    if {![db_column_exists im_companies default_referral_source_id]} {
        db_dml add_referral_source "alter table im_companies add column default_referral_source_id integer references im_categories on delete set null"
    }

    if {![db_column_exists persons position]} {
        db_dml add_position "alter table persons add column position text"
    }

    cog::apm::after_upgrade -from_version_name 5.0.0.0.0 -to_version_name 5.3.0.0.0
}
